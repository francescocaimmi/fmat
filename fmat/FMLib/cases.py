# -*- coding: UTF-8 -*-
#see LICENSE.txt in the root source directory for licensing information

"""
Cases module provides the LoadCase class, a class for storing
experimental data, their labels and type.

The experimental data are stored into numpy arrays, of different type for
control variables (X-data) and output variables (Y-data). A Load case is
associated with a **single** output variable.

Load Case Definition
====================

A loadcase is defined in the input file by a section

**Input file usage**: ``[LoadCase`n`]``
                        where n is a progressive number

A name can be attached to the load case, if none is given a default unique name
will be assigned.

**Input file usage**: ``name= namestring``

X-data
------

X-data, that is control variables.
These are stored in a an array like structure.
The outher index identifies a series of
"tables" storing a `jxk` array where the actual values of multiple control
variables are stored. There is no need for `j` and `k` to be same for every `i`
This structure is sufficiently flexible to account for
most experimental situation.

In the most simple case the array will be of the form `1xjx1`: a single table
containing j measurements of a single control variable, `e.g.`  strains at
varying time in an uniaxial experiment.

If the load-case response is determined by more than a single control variable,
then the array will be two dimensional (`k`!=1), with as many columns as needed.
An example may be strain in two directions, taken at varying times, for the
identification of an hyper-elastic material in a biaxial stress state.

A third example may be identification driven by digital image correlation data,
maybe in conjuction with FE procedures. In this case the model response is
driven by space coordinates (`jxk`) and measured at different, discrete times
(`i` index). A similar case would be that of a viscoelastic material with
measurements at different temperatures.

In principle, as there is no limit to the lenght of the axis, more complex
situations can be envisaged, for instance situation where there is an ordering
parameter for state variable measurements (axis `j`) and more than one relevant
functional parameter (axis `i`). This can be andles by adding more data in
the structure but the way this is done must be handled at a model level.

Each model response fuction will treat differently such a data structure.
For instance, the linear elastic model in the case of uniaxial data needs only
a `1xjx1` array with j strain measurements; in the case of Poisson response
function it needs data of the same kind. Be sure to read the documentation
pertaining to the model in order to know how to specify the data; mistakenly
specifying them may result in unexpected behaviour.


Y-data
------
Y-data, that is output variables.
For each load case, it is assumed that a single  output variable is measured.
If experimentally the measurements involve more than one output variable,
one should define as many load cases as there are output variable,
with the same control variable data. In this way different
response functions for different load-cases may also be specified as required,
for example, to identify the Poisson's ratio from uniaxial data.

Thus,speeding up the calculations,Y-data these are stored in a single array,
made up various blocks corresponding to the `i`-th entry of the

Material models must be aware of this difference.

Weights
-------

Weights can be assigned to Y-Data. In order to do this one must add, at the
end of the data files, a column of weights and flag the use of weights in
loadcase definition. The (squared) data themselves can be used as weights
by specifying the ``data`` value for the ``weights`` option, which otherwise
accepts ``True`` and ``False`` values.

For batch use this is done as follows:

**Input file usage**: ``weights = True|False|Data``

if no option is specified it is assumed that it defaults to False

Data Import
-----------

Data are imported into the LoadCase in two ways: by passing directly an object
that can be converted into a numpy array of the proper shape [#f1]_ or reading
from files. The former method is supported in batch mode by direct insertion
of the data into the input file and will be by copy paste in GUI mode (when
implemented)

At the present time the only file format supported is one that is understood
by numpy genfromtxt with whitspace separators (tabs will do), no headers, no
footers. Comments are allowed as always through the `#` character.

In the future the list of supported files will increase.

As it is more common to store data for output and control variables in a single
file and in the case of field data measured at differente times, to have
a single file for each time (or temperature or whatever), the LoadCase object
will take as input a list of files (in the simplest case, a single file), to be
specified in the input file or via GUI.
The columns of the input data will be intepeted as Y-data values for the last
column and X-data values for the other columns

**Input file usage**: ``src = file1, file2, file3...filen``

Data Labels
-----------

A label can be attached to the data; it will be used for graphic output.
As for data files this can be given as comma seprated list  or as a single
entry. Latex formatting is allowed.

**Input file usage**: ``x_label | y_label = label | label1,label2,label3``

Load Case Type
--------------

A type must also be associated with each load case. Types are used to tell the
model from which experiment do the data come from and hence how to handle the
X-data to calculate the Y-data. The reader is referred to model packages docs
to see which types are supported by each model.

**Input file usage**: ``type = supported_type``

.. rubric:: Notes
.. [#f1] The Class uses Python **eval** to do the conversion

Additional loadcase attributes specifications
-----------------------------------------------

The user can attach further attributes to the loadcase object by specifing
them in the input file. No type conversion will be attempted so the options
will be understood as strings. This feature can be usefull with user defined
inputs

**Input file usage**: ``user_defined_name = string``


LoadCase Class
==============



"""
#*******************************************************************************
#Author Information
#Francesco Caimmi, Ph.D.
#
#francesco.caimmi@polimi.it
#Skype: fmglcaimmi
#GPG Public Key : http://goo.gl/64dDo
#see LICENSE.txt in the root source directory for licensing information

#
#imports
import logging
import numpy as np
try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict
import sys
#FMAT imports
from fmat.FMLib import FMglobs

#prepare the logger
fmlog = logging.getLogger(FMglobs.FMLOGGER)

class LoadCase:
    """

    Class containing the experimental data


    Parameters
    ----------

    name: string
        loadcase identifier

    src: (varies)
        data source. Valid options are:

        * file or list of files: (string)
            read data from a file identified by a string or a comma-separated
            list of strings
            Files shall be white spaces (or tab) separated.
            Only the first two columns are read. Comments may be inserted
            via '#' character.


        * list:
            a python list or a 2xn numpy array

    Returns
    -------

    name, is_active, x_raw, y_raw, x_label, y_label:
        this parameters are set by init

    Attributes
    ----------


    is_active: boolean
        a flag to determine if loadcases shall be written to input
        files or used in the identification ; only useful in interactive modes

    x_raw:   multidimensional numpy array
            array containing the input data (control variables)
            shape(`ntables`,`ndata`,`nvars`)

    y_raw:numpy array
            numpy array
            shape(`ndata`*`ntables`)

    x_label: list of strings
        label for x data (e.g. strain, time etc.)

    y_label: list of strings
        label for y data (e.g. force, stress etc.) (string)

    x:  multidimensional numpy array
        x-data on which the fit will be performed. May be different from raw
        data. Shape(`ndata`,`nvars`)

    y:  numpy array
        y-data on which the fit will be performed. May be different from raw
        data. Shape(`ndata`)

    w: numpy array
        a diagonal matrix of weights for the experimental data
        Defaults to the Identity matrix if weights = False is specified in
        the init method call
        Shape(`ndata`,`ndata`)

    t_type: string
        test type (e.g. unixial, biaxial,etc)




    Methods
    -------

    inactive()
        sets as inactive

    trim data
        trims the data reducing the number of active data


    """

    def __fromfile(self, source, weights):
        """
        Private

        Reads data from a file, using numpy genfromtxt
        with whitespace sequence delimiter, so tab separated files should
        work. The last column is interpreted as y data; the others as x data

        Parameters
        ----------

        source:   string
            the name of the file from which to read or a string containing a
            list of files

        weights: boolean
            a boolean specifying whether weights for Y-data are specified in
            the data file

        Returns
        -------

        x:  numpy array like objects
            array containing the experimental data for control variable.
            The type of the array is object and it's shape is (len(source)).
            Every element of the array is an array with shape determined by
            the data given in the input file(s).

        y:  numpy array
            array containing the experimental data for output variable,
            packed in a single list made of the elements taken from the data
            file(s).



        """
        source = source.split(',')
        #remove trailing spaces
        for index in xrange(len(source)):
            source[index] = source[index].strip()
        #init temp variables
        y = np.array([])
        w = np.array([])
        datalist = list()

        for filename in source:
            data = np.genfromtxt(filename, unpack = True)
            #weights handling. If weights are not given ones will be
            #used by default
            if weights == True:
                y = np.append(y, data[:][-2])
                datalist.append(data[:][:-2])
                w = np.append(w, data[:][-1])

            #use the data themselves as weights
            elif weights in ['Data','data']:
                y = np.append(y, data[:][-1])
                datalist.append(data[:][:-1])
                w = np.append(w, np.power(np.where(y!=0,np.divide(1,y),1),2))

            else:
                y = np.append(y, data[:][-1])
                datalist.append(data[:][:-1])
                w = np.append(w, np.ones(len(data[:][-1])))

            del data


        #check to see if the datalist is empty
        if not datalist:
            fmlog.info('The datas, i.e. the xs, appear to be empty. Check input')
            if weights == True:
                fmlog.info('Maybe weights were incorrectly specified ')
            sys.exit(1)

        self.w = np.diag(w)
        self.y_raw = y
        self.x_raw = np.array(datalist)
        self.x = self.x_raw.copy()
        self.y = self.y_raw.copy()

    def __init__(self, src, name = None, weights = False ):
        """

        Load Case init function


        Parameters
        ----------

        name: (string)
            loadcase identifier

        src:
            data source. Valid options are:

            file:
                read data from a file identified by a string
                Files shall be white spaces (or tab) separated.
                Only the first two columns are read. Comments may be inserted
                via '#' character.


            list:
                a python list or a 2xn numpy array

        weights: boolean
            flag specifing whether weights for Y-data are specified in the
            source

        Returns
        -------

        Sets:
            name, is_active, x_raw, y_raw, x_label, y_label, w

        """
        self.name = name
        self.is_active = True

        #check to read the data
        if type(src) == str:
            self.__fromfile(src, weights)
        else:
            #this is a palceholder for further developments supporting input
            #from sources different from files
            self.x_raw = src[:][0]
            self.y_raw = src[:][1]
            self.x, self.y = self.x_raw, self.y_raw
        #valori di default per etichette dati e tipo test
        self.x_label = "x"
        self.y_label = "y"
        self.t_type = "uniaxial"

        #weights

    def trimdata(self, minindex = 0, maxindex = -1):
        """
        Trims the active data between two index values
        Only the data between such index will end up in the fitting procedure
        Used only in interactive mode.At present it is very outdated

        Parameters
        ----------

        minindex, maxindex : integers
            integers specifying the trimming boundaries

        Returns
        -------

        self.x, self.y:
            values used in actual computations

        """
        #TODO trim for weights and update the fuction to comply with the new
        #case structure
        for key in self.x_raw:
            self.x[key] = self.x_raw[key][minindex:maxindex]
        self.y = self.y_raw[minindex:maxindex]

    def inactive(self):
        """
        Set the LoadCase inactive. It will thus not be used in input
        generation or model solution.
        """
        self.is_active = False
