# -*- coding: UTF-8 -*-
#author: Francesco Caimmi francesco dot caimmi @ polimi dot it
#see LICENSE.txt in the root source directory for licensing information
"""
.. _fmglobs_doc:

FMglobs documentation
=====================

Defines global varibles for FMat, mainly list of valid test identifiers
and valid model load cases available.

Some global option such as verbose flag are also set here.

Global variables
------------------

FM_CASES_VALID
    the list of valid load cases

FMLOGGER
    the name of the logger object

FM_MODELS_AVAILABLE
    a dictionary associating

FM_SOLVERS_OPENOPT
    the list of solvers from openopt already wrapped up

OPERATION_MODE_DICTIONARY
    a dictionoary with the available operations mode


Global options
--------------
These are set by the program at exectuion time or at the command line

FMsolver_name
    the name of the solver which is going to be used

FMverbose
    a boolean option for verbose output. If `True` also sets the logging level
    to DEBUG

"""
#Python Imports

import ConfigParser
import sys


#
BASE_PATH = sys.path[0]+'/'

#Logger name
FMLOGGER = 'fmlog'

#dictionary of operation modes available
OPERATION_MODE_DICTIONARY = {'batch': 'FMbatch',
                             #'CLI':'FMCli',
                             #'GUI': 'FMGui',
                             'compare': 'FMcompare'
                             }
#Valid cases types
FM_CASES_VALID = ['uniaxial',
                  'biaxial',
                  'poisson',
                  'pure shear',
                  'genbiaxial',
                  'generic',
                  #viscoelastic response functions
                  'relaxation_modulus',
                  'storage_modulus',
                  'loss_modulus',
                  'loun',#loading unloading
                  ]

#Dictionary of available material models
#for each material model name associates the corresponding module to load
FM_MODELS_AVAILABLE = { #elastic materials
                        'Hooke':"fmat.models.elastic.FMhooke",
                        'Ogden':"fmat.models.elastic.FMogden",
                        'elpol': "fmat.models.elastic.FMpolynomial",
                        'eightchain':"models.elastic.FMArrudaBoyce",
                        'rpol':"fmat.models.elastic.FMredpoly",
                        #ABAQUS materials
                        'ABAQUS_cohesive':"fmat.models.ABAQUS.FMabq_cohesive",
                        'ABAQUS_ogden': "fmat.models.ABAQUS.FMabq_ogden",
                        #viscoelastic materials
                        'BB':"fmat.models.viscoelastic.FMBB",
                        'Prony':"fmat.models.viscoelastic.FMprony",
                        #user materials
                        'Generic':"fmat.models.FMgeneric"
                       }


#list of solvers for every solver package supported
FM_SOLVERS_PYOPT = ['NSGA2', 'SLSQP','ALPSO','ALHSO','SDPEN']
FM_SOLVERS_OPENOPT = ['DE','RALG','SCIPY_COBYLA', 'SCIPY_LBFGSB','INTERALG' ]

#list of available solvers
FM_AVAILABLE_SOLVERS = FM_SOLVERS_PYOPT + FM_SOLVERS_OPENOPT


#external command paths read from configuration file
cfginput = ConfigParser.ConfigParser()
cfginput.read(BASE_PATH+"FMatConfig.cfg")

#abaqus command to use for FEM driven optmization
abaqus_cmd = None

for section in cfginput.sections():
    try:
        abaqus_cmd = cfginput.get(section,'abaqus_cmd')
    except ConfigParser.NoOptionError:
        pass

###############
#global options
###############

FMverbose = False

FMsolver_name = ''

FMstatistics = False #statistic output flag


###################
#Flags for warnings
###################
#flag for outputting warnings for suspect data


FM_suspect_data_number = False

#flag for no post_processing script in FE linked analysis

FM_no_post_script = False