# -*- coding: utf-8 -*-
#see LICENSE.txt in the root source directory for licensing information

"""
FMlogger initiates and configure the logger facilities. The name of the logger
is provided by the global variable FMLOGGER, available in the module FMglobs.
For further information see :ref:`fmglobs_doc`.

"""
#Python imports
import logging

#FMat imports
import FMglobs

def initLogging(verbose = False):
    """
    Creates the logger object

    Parameters
    ----------

    verbose: boolean
        if True, the defualt logging level is set to DEBUG. If false is set to
        INFO. Dafults to False

    Returns
    -------

    None
    """

    # create logger
    logger = logging.getLogger(FMglobs.FMLOGGER)

    # create console handler and
    ch = logging.StreamHandler()

    #set the loggin level
    if verbose:
        ch.setLevel(logging.DEBUG)
        logger.setLevel(logging.DEBUG)
    else:
        ch.setLevel(logging.INFO)
        logger.setLevel(logging.INFO)

    # create formatter
    formatter = logging.Formatter('%(asctime)s - %(levelname)s - %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)
