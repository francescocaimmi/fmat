# -*- coding: utf-8 -*-
#see LICENSE.txt in the root source directory for licensing information

"""
Author Information
Francesco Caimmi francesco.caimmi@polimi.it

Wrapper around interalg solver from OpenOpt package to be used by FMOpenOpt.

For details on the solver itself, the reader is referred to `OpenOpt website
<http://http://openopt.org/Welcome>`_

The separate wrapper is needed as it must overload a lot of functions
using imports from FuncDesigner modulus.

"""

#Python imports
import copy

try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

#from numpy import dot


#third party imports
#from FuncDesigner import *

#FMat imports
from fmat.FMLib import FMglobs


def interalg_obj_wrapper(loadcase_dic, model):
    """
    Wrapper to build the objective function for interalg


    Parameters
    ----------


    model: material model object

    loadcase_dic: dictionary
        dictionary of loadcases cotaining instances of cases class

    Returns
    -------

    f: float
        objective function value

    """
    def obj(x):
        """
        Objective function

        Parameters
        ----------

        x: array like
            placeholder for the parameter values

        Returns
        -------

        f:  float
            objective function value
        """
        #make a copy of the model object. You never know...
        model_copy = model
        print x

        i = 0
        for key,value in x.iteritems():
            model_copy.parameters[key] = value
            i += 1

        if i != model.n_par:
            print "WARNING:free parameters at model level differ from \
            parameters supplied by the objective function"

        RSS = 0

        for loadcase in loadcase_dic.itervalues():
            model_copy.evaluate(loadcase)
            #using the model result lenght there is no risk to have loadcase.y
            #longer than the results

            RSS += (1.0/2.0)*norm(
                dot(loadcase.w,
                (model_copy.result-loadcase.y[:len(model_copy.result)]))
                , 2)**2

        f = RSS

        #verbose output
        if FMglobs.FMverbose == True:
            print "Parameters"
            print(model_copy.parameters)
            print "Residue:",f

        return f

    return obj


def eFunc(x,*args):
    #make a copy of the model object. You never know...

    loadcase_dic, model,startPoint = args
    model_copy = model

    i = 0
    for key in model_copy.free_parameters.keys():
        model_copy.parameters[key] = x[i]
        i += 1

    if i != model.n_par:
        print "WARNING:free parameters at model level differ from \
        parameters supplied by the objective function"

    RSS = 0
    for loadcase in loadcase_dic.itervalues():
        model_copy.evaluate(loadcase)
        temp = (model_copy.result-loadcase.y)**2
        print type(temp)
        RSS = sum(temp)
#        temp = dot(loadcase.w,(model_copy.result-loadcase.y))
#
#        RSS += (1.0/2.0)*dot(temp,temp)

    #verbose output
    if FMglobs.FMverbose == True:
        print "Parameters"
        print(model_copy.parameters)
        print "Residue:", RSS(startPoint)

    return RSS


def main(loadcase_dic, model, problem,solver):
    """
    Main function that solves the optimization problem

    Parameters
    -----------

    loadcase_dic: Dictionary
        a dictionary of loadcases object defining the problem to be solved

    model: material model object

    problem: OpenOpt problem instance

    solver: OpenOpt solver instance
    Returns
    --------
    """
    print 'INTERALG support is at present very limited. Use at your risk'

    #special code to handle interval analysis by interalg
    #define variables. We use oovars, basically some kind of object defined by
    #OpenOpt

    FMvars = OrderedDict()
    i = 0
    for key in model.free_parameters:
        FMvars[key]=oovar(key,
                            lb = model.bounds[i,0],
                            ub = model.bounds[i,1])
        i += 1


    x = oovars(len(model.free_parameters.keys()))
    for i in range(len(model.bounds)):
        x[i].lb = model.bounds[i,0]
        x[i].ub = model.bounds[i,1]
    #define starting values

    startPoint = copy.deepcopy(model.free_parameters)
    i = 0

    for key in startPoint:
        x[i].name = key
        startPoint[key] = model.start[i]
        i += 1

    startPoint = dict()
    i = 0
    for elem in x:
        startPoint[elem] =  model.start[i]
        i +=1


    problem.x0 = startPoint

    #define upper and lower bounds. These are mandatory and definde in model
    #class. Due to the use of oovars these are defined as box constraints
#    constraints = [FMvars[i] < model.bounds[i,1] for i in  xrange(len(FMvars))]
#    constraints += [model.bounds[i,0] < FMvars[i] for i in xrange(len(FMvars))]
#    problem.constraints = constraints
#    print problem.constraints
    f1 = eFunc(x,loadcase_dic, model,startPoint)
#    print = f1(startPoint)
#    a = f([startPoint])
#    print a
    problem.f = f1

    solution = problem.solve(solver)

    return solution
