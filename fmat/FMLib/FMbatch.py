# -*- coding: utf-8 -*-
"""
FMbatch is the module containing the top level functions
for batch operating mode, which builds project objects from options specified
in the input file, which uses Config Parser syntax

Routine Listing
----------------

"""
#Python imports
import sys
import ConfigParser
import numpy as np
try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict
import logging


#FMat imports
from cases import LoadCase
import FMglobs
from fmat.solvers import FMpyOpt
from fmat.solvers import FMOpenOpt
import FMplot
import FMstats
from RSS import RSS
from RSS import RSSgradient

#prepare the logger
fmlog = logging.getLogger(FMglobs.FMLOGGER)

def model_vs_cases(model, cases_dict):
    """
    Check if load cases and model are compatible

    Parameters
    ----------

    model: material model class instance

    cases_dict: dictionary
        loadcase dictionary

    Returns
    -------

    none:
        aborts execution if error is found

    """
    error_flag = False
    for key, value in cases_dict.iteritems():
        case_type = value.t_type
        if  case_type not in model.RES_FUNC:
            error_flag = True
            error_string = case_type
            break

    if error_flag == True:
        fmlog.error("ERROR: " + error_string +
                    "load case is not supported by current model")
        sys.exit(666)




def build_cases(parser, verbose = False):
    """

    For the batch operation mode builds the LoadCases dictionary

    Parameters
    ----------

    parser:
        config parser object

    verbose: boolean
        flag for verbose output

    Returns
    -------

    loadcase dictionary: (dictionary)
        a dictionary containing all of the loadcases object according to the
        scheme {`loadcase name`:`loadcase object`}

    num: integer
        number of load cases

    """
    #list of standard loadcase attributes
    #user can define additional attributes
    cases_attrs = ['name', 'weights', 'src', 'x_label', 'y_label', 't_type']
    num = 0 #number of load_cases
    loadcase_dic = OrderedDict() #dictionary to store the load cases

    # parse the sections
    for section in parser.sections():
        #it is a LoadCase and defines a new LoadCase
        if 'LoadCase' in section:

            try :
                LoadCase_name = parser.get(section, 'name')
            except ConfigParser.NoOptionError:
                LoadCase_name = "LoadCase"+str(num+1)

            try:
                LoadCase_weight = parser.get(section, 'weights')
                if LoadCase_weight == 'True':
                    LoadCase_weight = True
            except ConfigParser.NoOptionError:
                LoadCase_weight = False

            try :
                LoadCase_src = parser.get(section, 'src')
                num+=1
                #instatiate the loadcase
                loadcase_dic[LoadCase_name] = LoadCase(
                                                LoadCase_src,
                                                name = LoadCase_name,
                                                weights = LoadCase_weight)
            except ConfigParser.NoOptionError:
                msg = "for Load Case" + LoadCase_name + "no data were \
                specified. Setting it as inactive"
                fmlog.info(msg)
                loadcase_dic[LoadCase_name] = LoadCase(
                                                    [[0],[0]],
                                                    name = LoadCase_name)
                num+=1
                loadcase_dic[LoadCase_name].inactive()
            #data labels assignment
            #se non specificate non fa nulla e usa le etichette di defualt x
            #e y (vedasi cases.py)
            try:
                loadcase_dic[LoadCase_name].x_label = \
                    parser.get(section, 'x_label').split(',')
            except ConfigParser.NoOptionError:
                if verbose == True:
                    print "no label for x data in LoadCase ", LoadCase_name

            try:
                loadcase_dic[LoadCase_name].y_label = \
                    parser.get(section, 'y_label').split(',')
            except ConfigParser.NoOptionError:
                if verbose == True:
                    print "no label for y data in LoadCase ", LoadCase_name

             #test type assignment
             #if not specified resorts to default (see cases.py)
            try:
                loadcase_dic[LoadCase_name].t_type = parser.get(section,
                                                                 't_type')
            except ConfigParser.NoOptionError:
                print "WARNING: no type specified for LoadCase ",LoadCase_name
                print "It will be assumed to be uniaxial"

            #check for user defined attributes
            for option, value in parser.items(section):
                if option not in cases_attrs:
                    setattr(loadcase_dic[LoadCase_name],option,value)

    return loadcase_dic, num

def build_model(parser, cases_dict, verbose = False):
    """
    Builds a Material model object

    Parameters
    ----------

    parser: config parser object

    cases_dict: dictionary
        dictionary of load cases

    verbose: Boolean
        flag for verbose output

    Returns
    -------

    material:
        model object

    num:    integer
        number of free parameters


    """
    model_flag = False #error flag
    model = None
    # parse the sections
    for section in parser.sections():

       if 'Model' in section:

            try :

                model_name = parser.get(section, 'name')

            except ConfigParser.NoOptionError:

                fmlog.error("No type specified for the material model")
                sys.exit(666)

            if model_name not in FMglobs.FM_MODELS_AVAILABLE.keys():
                msg = "ERROR no material named " + model_name + " is supported"
                fmlog.error(msg)
                sys.exit(666)



            #define the module to which the class needed belongs
            #these are stored in the dictionary FM_MODELS_AVAILABLE
            model_module = __import__(
                        FMglobs.FM_MODELS_AVAILABLE[model_name],
                        fromlist=[''])
            #instatiate the class; if the model is 'Generic', i.e. user defined
            #it needs a special treatment
            if model_name == 'Generic':
                max_param = int(parser.get(section,'MAX_PARAM'))

                res_functions = parser.get(section, 'RES_FUNC')
                rs = {}
                for elem in res_functions.split(','):
                    tmp = elem
                    tmp = tmp.translate(None, "'{}")
                    key, val =tmp.split(':')
                    rs[key] = val

                model = model_module.__getattribute__(model_name)(max_param,
                                                                    rs)
            else:
                model = model_module.__getattribute__(model_name)()
            #check for fimodel = eval('model_module.' + model_name + '()')xed parameters in the input file
            fixed_parameter_dic = OrderedDict()
            for parameter in model.PARAMETER_NAMES:
                try:
                    fixed_parameter_dic[parameter] = \
                    parser.get(section, parameter)

                except ConfigParser.NoOptionError:

                    fmlog.debug( "Parameter " + parameter+
                                " not specified in input.")
                    fmlog.debug( "Assuming it to be free")

            if verbose == True:
                print "Fixed parameters dictionary ", fixed_parameter_dic

            model.set_parameters(fixed_parameter_dic)


            #define the parameter bounds and init values
            try:
                bounds = parser.get(section, 'bounds')
                #convert to list
                bounds = eval(bounds)
            except ConfigParser.NoOptionError:
                bounds =[]
                fmlog.debug("No bounds given for the parameters."+
                            "Defaults will be used")

            try:
                init = parser.get(section, 'init')
                #convert to list
                init = eval(init)
            except ConfigParser.NoOptionError:
                init = []
                fmlog.debug("No initial values given for the parameters."+
                            "Defaults will be used")

            model.set_param_space(bounds = bounds, init = init)

            #define constraints
            constraint_items = parser.items(section)
            constraint_items = [pair[1] for pair in constraint_items if \
                                'constraint' in pair[0]]
            model.set_constraints(constraint_items)
            model_flag = True

    #check if model is ok for the loadcases defined

    model_vs_cases(model,cases_dict)


    if model_flag == False:

        fmlog.error("No model specified in input file.\nExiting")
        sys.exit(666)

    return model, model.n_par


def solve(parser, cases_dict, model, verbose):

    """
    Solves the optimization problem

    Default solver is NSGAII from pyOpt

    Parameters
    ----------

    parser: config parser object
        contains the options from the input file

    cases_dict: dictionary
        dictionary of load cases

    verbose: Boolean
        flag for verbose output

    model: model object

    Returns
    -------

    model: model object
        updated versione of the model object with parameter values set equal
        to the optmization values

    result: dictionary

        a dictionary containing the following {`key`:`pair`} data

        `evals`:
            number of function evaluations
        `min`:
            objective function value corresponding to the minimum
        `parameters`:
            dictionary of parameters following the same scheme as
            the one used for the model objects
        `time`:
            execution time

    solver_name:    string
        solver name

    """
    #read the solver name and check if it exists
    if not(parser.has_section('Solver')):
        solver_options={}
    else:
        solver_options={}
        #build a dictionary of solver options to be passed to the solver
        for option in parser.options('Solver'):
            solver_options[option]=parser.get('Solver',option)
        solver_options.pop('name')

    #load the appropriate solver module

    if FMglobs.FMsolver_name in FMglobs.FM_SOLVERS_PYOPT:
        solver_module = FMpyOpt
    elif FMglobs.FMsolver_name in FMglobs.FM_SOLVERS_OPENOPT:
        solver_module = FMOpenOpt
    else : #solver not supported
        logging.error("The specified solver is not supported. Quitting.")
        sys.exit(666)

    #export the model object and the dictionary of load cases


    
    model, result = solver_module.find_solution(solver_options,
                                           cases_dict,
                                           model,
                                           FMglobs.FMverbose)

    #assign the parameters to the free parameters dict too
    for key in model.free_parameters:
        model.free_parameters[key] = model.parameters[key]
    return model, result


def stat_output(cases_dict, model):
    """
    Function providing output for the statistical calculations.

    Parameters
    ----------

    cases_dict: dictionary
        dictionary of load cases

    model: model object

    Returns
    -------

    if evaluation of something fails, it returns none
    """
    fmlog.debug('Starting Statistical Output')
    #calculate the gradient of the RSS
    Sgrad = RSSgradient(cases_dict, model)
    #calculate the covariance matrix
    cov = FMstats.cov_matrix(cases_dict, model,Sgrad)

    if cov == None:
        #no stats can be computed
        fmlog.info("Further statistical calculations will be suppressed")
        return None
    #save the covariance matrix
    np.savetxt('covariance.txt',cov)
    #calculate and output the error estimates for the parameters
    err = np.sqrt(np.diag(cov))
    fmlog.info('Estimated parameters')
    with open('parameters_estimates.txt','w') as fout:
        for index, key in enumerate(model.free_parameters):
            outstring = key+': '+str(model.free_parameters[key])+' +/- '+\
                        str(err[index])
            fout.write(outstring + '\n')
            fmlog.info(outstring)
    #calculate the error estimates for the predicted data points
    Sigma_f = np.zeros(Sgrad.shape[0])
    for i in xrange(len(Sgrad)):
        sigma = np.sqrt(np.tensordot(np.outer(Sgrad[i],Sgrad[i]),cov))
        Sigma_f[i] = sigma

    #append the error column to the output files
    y_index = 0
    for case in cases_dict.itervalues():
        for index, dataset in enumerate(case.x):

            num_ds_data = len(dataset[0])

            if len(case.x)!=1:
                input_file = str(index+1)+'_'+case.name
            else:
                input_file = case.name

            old_data = np.genfromtxt(input_file+'_predictions.txt',
                                 delimiter = '\t',
                                 )

            new_data = np.column_stack((old_data,
                                     Sigma_f[y_index:y_index+num_ds_data]))

            np.savetxt(input_file+'_predictions.txt',
                    new_data,
                    delimiter = '\t',)

            y_index += num_ds_data


def model_output(cases_dict, model):
    """
    Generates textual and graphics output of the predicitions of the model
    compared to the experimental datal

    Parameters
    ----------

    cases_dict: dictionary
        dictionary of load cases

    model: model object

    Returns
    -------

    graphic files: some
        name is given by case name
        Default format is .pdf

    text file:
        name is case name + '_predictions.txt'
        tab separated file contains data and predictions

    """

    #check if matplotlib is installed
    try:
        import matplotlib
        matplotlib.use('Agg')#this backend is totally not interactive
        matplotlib.rc('font', family = 'serif')
        from matplotlib import pylab as plt
        mplinst = True
        #set matplolib options
    except ImportError:
        fmlog.info('Seems that matplotlib is not installed on this system')
        fmlog.info('Graphics output will be skipped')
        mplinst = False



    for case in cases_dict.itervalues():
        #this index keeps tracks of the number of y data already processed
        y_index = 0
        #must evaluate the model: exact paramameters may not be correspond
        #to the last optimizer evaluation
        model.evaluate(case)
        for index, dataset in enumerate(case.x):

            #save the data
            temp = dataset.tolist()
            temp.append(model.result)
            data = (np.vstack(temp)).transpose()
            #build the filename for output
            #you don't want to append the index if there is only one
            #set of data

            if len(case.x)!=1:
                output_file = str(index+1)+'_'+case.name
            else:
                output_file = case.name

            np.savetxt(output_file+'_predictions.txt',
                   data,
                   delimiter = '\t',
                   )

            #plot the data
            if mplinst == True:
                plt.figure()

                #chek that data can actually be plotted
                if len(dataset)==1:
                # data consist of a single control variable
                    plt = FMplot.fm_xy_compare(plt,
                                    dataset[0],
                                    case.y[y_index:y_index+len(dataset[0])],
                                    model.result[y_index:y_index +
                                                        len(dataset[0])],
                                    case.x_label[index],
                                    case.y_label[index]
                                            )

                    plt.title(case.name)
                    plt.savefig(output_file+'.pdf', bbox_inches='tight')

                elif len(dataset) == 2:

                    plt = FMplot.fm_contour_compare(
                        plt,
                        dataset,
                        case.y[y_index:y_index+len(dataset[0])],
                        model.result[y_index:y_index+len(dataset[0])],
                        case.x_label[index],
                        case.y_label[index]
                                            )
                    plt.title(case.name)
                    #save
                    plt.savefig(output_file+'.pdf', bbox_inches='tight')

            y_index += len(dataset[0])

    return None


def set_solver_name(parser):
    """
    sets the global variable FMsolver_name

    Parameters
    ----------
    parser:    config parser object

    Returns
    -------
    none

    """
    #read the solver name and check if it exists
    if not(parser.has_section('Solver') and
        parser.has_option('Solver','name')):
        solver_name = 'NSGA2'
    else:
        solver_name = parser.get('Solver','name')
    FMglobs.FMsolver_name = solver_name
