# -*- coding: utf-8 -*-
"""
Defines the basic material model class for FMat

It is inherithed by other classes defining material behaviour

The basic interface for the evaluation of the material response is defined by
the *evaluate* method, which passes to the response function a loadcase object
and a model object

"""
#Python imports
import random
import sys
try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict
import numpy as np
import logging

#FMAt imports
from fmat.FMLib import FMglobs

#get the logger
fmlog = logging.getLogger(FMglobs.FMLOGGER)




###########################
#Model Class
###########################
class Model:
    """

    .. _model_attributes:

    Attributes
    -----------

    NAME:   string
            Class Name

    MAX_PARAM:  integer
                maximium number of parameters, free+fixed

    PARAMETER_NAMES:    list of strings
                        list of the parameter names.
                        In the input file the starting parameters
                        shall be specified according to the  list,
                        skipping parameters for which fixed values are given.

    RES_FUNC:   dictionary
                a dictionary providing the link between the admissible
                load cases for the material and the response function
                to calcluate their response from the data for
                such a load case. For instance for Hooke's
                linear elastic material it something like
                ``{'uniaxial':hooke_uniaxial, 'poisson': hooke_poisson}.``
                It is also used to check that the load cases
                types are actually available for the specified material model.


    RSS: float
        The squared residual sum for the model evaluated for the current
        set of model parameters

    n_par:  integer
        the number of free parameters

    bounds: list
        bounds on the paramters in the search space
        specified as
        [[x_1_min,x_1_max]...[x_n_params_min,x_n_params_max]]
        (list of list of reals).
        Defaults to [-100,100] n_params times

    start: list
        list of starting parameters values (list of reals)
        defaults to random numbers in the real interval [-100,100]

    fail:  integer
        a flag to determine if the last evaluation was succesfull
        It is 0 for success and one for fail (follows pyOpt conventions)

    constraints:    dictionary
        dictionary of lists defining the constraints to be placed
        on the parameters. These must be specified as
        [parameters_combination,'e'|'i'] where
        parameter_combination is a string containing the constraint
        expressed as parameter_combination < | = 0. Wether the
        constraint is equality or inequality is defined by the
        string 'e' or 'i' in the second element of the list.
        In input file every constraint must be specified as
        constraint<number> = [parameters_combination,'e'|'i'].
        The dictionary itself is defined by the followng structure
        {constraint<number>:[parameters_combination,'e'|'i']}

    fixed_parameters: dictionary
        dictionary of fixed parameters according to the followig scheme
        {PARAMETER_NAME[i]:fixed value}

    free_parameters: dictionary
        dictionary of free parameters according to the
        followig scheme {PARAMETER_NAME[i]:value to be set by the optimizer}

    parameters: dictionary
        union of the free and fixed parameters dictionary for use
        in response evalutaion function



    Methods
    -------

    evaluate:
        Evaluates material response

    OBJ:
        the objective function as a function of the free parameters. Can be
        called to get the value of the objective function. As the actual
        evaluation of the objective function depends on the solver, it is the
        solver responsability to set it during its setup. It initially is set
        to `None`

    set_param_space:
        Defines the bounds on the parameter
        space region that can be accessed by the solver

    set_parameters:
         Builds the material parameters dictionaries

    set_constraints:
        Builds the constraints dictionary

    """

    PARAMETER_NAMES = []

    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC = dict()

    NAME = ''

    RSS = None

    def __init__(self):
        self.constraints = OrderedDict()
        pass


    def set_param_space(self, bounds = [], init =[]):
        """
        Defines the bounds on the parameter space region that can
        be accessed by the solver by setting model.bounds and model.start

        Parameters
        ----------

        bounds : list of floats
            bounds on the paramters in the search space specified as
            [[x_1_min,x_1_max]...[x_n_params_min,x_n_params_max]]
            Defaults to [-100,100]

        init : list of reals
            list of starting values for the solver intialitazion


        Returns
        -------

        self.bounds: list of floats
            if not given as input,defaults to random numbers in the
            real interval [-100,100]

        self.init: list of reals
            if not given as input,defaults to random numbers in the
            real interval [-100,100]

        """
        if not bounds:
            for i in range(self.n_par):
                bounds.append([-100, 100])

        if not init:
            for i in range(self.n_par):
                init.append(random.uniform(-100, 100))

        self.bounds = np.array(bounds)
        self.start = np.array(init)

    def evaluate(self, loadcase):
        """
        Evaluates material response

        Parameters
        ----------

        loadcase: a LoadCase class instance defined in cases.py

        Returns
        -------
        self.result: numpy array
            array of responses

        """
        self.result = self.RES_FUNC[loadcase.t_type](loadcase,
                                                    self)
    def set_parameters(self, fixed_parameters):
        """
        Builds the material parameters dictionary

        Parameters
        ----------

        fixed_parameters: dictionary
            a dictionary containing keys corresponding to material
            parameters  and their fixed value

        Returns
        -------

        self.fixed_parameters, self.free_parameters, self.parameters:
            dictionaries
            dictionaries containing the free material parameters,
            the fixed ones and the union of the two

        """
        free_parameters = OrderedDict.fromkeys(self.PARAMETER_NAMES, None)

        for key in fixed_parameters:
            free_parameters.pop(key)
        #try to convert the parameters
            try:
                fixed_parameters[key] = float(fixed_parameters[key])
            except ValueError:
                pass

        self.free_parameters = free_parameters
        self.fixed_parameters = fixed_parameters
        self.parameters = OrderedDict(  self.free_parameters.items() + \
                                        self.fixed_parameters.items() )

        n_free = len(self.PARAMETER_NAMES)-len(fixed_parameters)

        if n_free <= 0:
            fmlog.error( "No free parameters left for identification")
            sys.exit(666)

        self.n_par = n_free

    def set_constraints(self,cons_list):
        """
        Builds the constraints dictionary

        Parameters
        ----------

        cons_list: list of strings

            a list of expressions specyfing the constraints. Every
            constraint is expressed as[parameters_combination,'e'|'i'] where
            parameter_combination is a string containing the constraint
            expressed as parameter_combination < | = 0. Wether the
            constraint is equality or inequality is defined by the
            string 'e' or 'i' in the second element of the list.

        Returns
        -------

        self.constraints:  dictionary
            a dictionary of constraints for further processing

        """

        for i in range(len(cons_list)):

            self.constraints['constraint'+str(i)] = \
            cons_list[i].lstrip('[').rstrip(']').split(',')


#############
#functions
#############
def warn_for_data(warn, loadcase):
    """

    this function prints a warning if the data supplied by user do not
    match those needed by the model by simply checking the lenght
    of the data structure

    Parameters
    ----------
    warn: int
        a number specifying that if there are a number of dataset for
        the loadcase which is greater than warn something might be wrong

    loadcase: LoadCase object

    Returns
    -------

    None:
        just print warnings on stdo
    """

    if len(loadcase.x)> warn and FMglobs.FM_suspect_data_number == False:
        fmlog.warning("WARNING: the number of input data files for load case"+
                        loadcase.name+
                        "is suspect, please check it"
                        )
        FMglobs.FM_suspect_data_number = True