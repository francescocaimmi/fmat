# -*- coding: utf-8 -*-
"""
Provides a model class to use with user defined response functions.

Model Definition and Usage
==========================

To use this class the user must provide the following options in the input file

MAX_PARAM
-----------

The number of parameters, both fixed and free that must be supplied to the
response functions for the model to work; fixed parameters can be specified
as usual one this option is given

**Input file usage**: ``MAX_PARAM = Hooke``

The parameters names will be stored as `x0 ... xi...xMAX_PARAMS`

RES_FUNC
-----------

a python dictionary containing keys corresponding to the load case
types specified in the input file and values corresponding to python modules
that contain a main function that accepts as inputs a loadcase object and a
model object.

**Input file usage**:``{'loacasetype':pythonmodule,'loacasetype2': pythonmodule}.``

One can also specify a function in a module specifying the dictionary value as
``pythonmodule.function_name``, where, once again, ``function_name`` accepts
a loadcase object and a model object as inputs.

"""
#Python imports
try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

#FMat imports
from fmat.models.FMmodels import Model

class Generic(Model):
    """
    Class that implements a user defined material

    See :ref:`model_attributes` for parameters common to all material
    models. Here only additional parameters will be listed.

    Parameters
    ----------

    max_param:  int
            the total number of parameters, both free and fixed.

    res_func: dict
        a dictionary associating loadcase types with the corresponding response
        function

    Returns
    -------

    as in parent class


    Attributes
    -----------------

    The same as the parent model class


    Other Parameters
    --------------------

    Material Parameters available are as listed in the constant
    `PARAMETER_NAMES` and are a list of the type `x0 ... xi...xMAX_PARAMS`
    """

    NAME = 'Generic'


    def __init__(self, max_param, res_func):
        self.constraints = OrderedDict()
        self.MAX_PARAM = max_param
        self.PARAMETER_NAMES = ['x'+str(i) for i in xrange(self.MAX_PARAM)]
        self.RES_FUNC = res_func


    def evaluate(self, loadcase):
        """
        Evaluates material response

        Parameters
        ----------

        loadcase: a LoadCase class instance defined in cases.py

        Returns
        -------
        self.result: numpy array
            array of responses

        """
        module_name = self.RES_FUNC[loadcase.t_type]
        if '.' in module_name:#a function in a module

            package_name, function_name = module_name.split('.')
            package = __import__(
                        package_name,
                        fromlist=[''])
            function = getattr(package, function_name)

            self.result = function(loadcase, self)

        else: #a module with a main function

            package = __import__( module_name, fromlist = [''])
            self.result = package.main(loadcase, self)