# -*- coding: utf-8 -*-
#see LICENSE.txt in the root source directory for licensing information

"""
Provides functions to perform a stastistical analysis of the results.

Most of the calculations are based on the book J. Wolberg (2006), "Data
Analysis Using the Method of Least Squares",Springer-Verlag Berlin Heidelberg.

Routine Listing
-----------------
"""
#Python imports
import logging

#Third party import
import numpy as np

#FMAT imports
import FMglobs
from RSS import RSS


#prepare the logger
fmlog = logging.getLogger(FMglobs.FMLOGGER)



def cov_matrix(cases_dict, model, gradient):
    """
    Calculates the covariance matrix of the fit i.e.

    .. math::

        COV_{ij}=\\frac{RSS}{N-n} C_{ij}^{-1}

    where

    .. math::
        C_{jk} = \sum_{i=1}^{N} W_{ii}\\frac{\\partial f_i}{\\partial a_j}
        (x_i)\\frac{\\partial f_i}{\\partial a_k}(x_i)

    where the sum runs over all data points, :math:`f_i` are the model
    response function, which may depend on the point under consideration and
    :math:`a_k` are the model parameters. There are `n` parameters.

    Parameters
    ----------
    cases_dict: dictionary
        dictionary of load cases

    model: model object

    gradient: ndarray
        array containing the gradient of the objective function w.r.t.
        parameters at each datapoint, as providede by the function RSSgradient

    Returns
    --------
    cov: nd.array
        nxn estimate of the covariance matrix, where n is the number of free
        parameters. The covariance matrix is multiplied by RSS/(N-n), where
        N is the number of data poitns, so that the error in the parameter
        estimates is just sqrt(inv(cov)). If inversion of the jacobian matrix is
        not possible returns None

    """
    N = 0
    n = model.n_par
    cov = np.zeros((n,n))
    S = RSS(cases_dict, model)
    #cycle on the load cases
    for case in cases_dict.itervalues():
        #get the number of parameters
        n_points = len(case.y)
        w = np.diag(case.w)
        #cycle over the case data
        for i in xrange(n_points):
            grad = gradient[N]
            cov += w[i]*np.outer(grad,grad)
            N = N + 1

    try:
        #the factor 2 coms from the fact that we divide the RSS by two
        cov = np.linalg.inv(cov)*2*S/float(N-n)
        return cov
    except np.linalg.LinAlgError:
        fmlog.info('Unable to invert the covariance matrix.')
        fmlog.info('A possible reason for this is that not all of the \
        parameters are actually used by the specified response functions')
        return None