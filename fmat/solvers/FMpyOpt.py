# -*- coding: utf-8 -*-
#see LICENSE.txt in the root source directory for licensing information

"""
Author Information
Francesco Caimmi francesco.caimmi@polimi.it

Wrapper around pyOpt solver module

The reader is referred to `pyOpt website <http://www.pyopt.org/index.html>`_
for a detailed description of the available solvers and their options


pyOpt is released under the terms of the GNU Lesser General Public License. You
can obtain its source code by visting
`its website <http://www.pyopt.org/index.html>`_
"""

#Python imports

import logging

#Third Party imports
try:
    import pyopt
except ImportError:
    import pyOpt


#Internal imports
from fmat.FMLib import FMglobs
from fmat.FMLib.RSS import RSS

#define the logger
fmlog = logging.getLogger(FMglobs.FMLOGGER)

def obj_wrapper(loadcase_dic, model ):
    """
    Wrapper to build the objective function

    For some reason the args are not passed by pyOpt down to the objective
    function.
    Therefore the needed stuff loadcase_dic and model_original
    are provided via an higher order function

    Parameters
    ----------


    model: material model object

    loadcase_dic: dictionary
        dictionary of loadcases cotaining instances of cases class

    Returns
    -------
    obj: callable object
        objective function callable object depending only on the parameters

    """
    def obj(x):
        """
        Objective function

        Parameters
        ----------
        x: array like
            placeholder for the parameter values

        Returns
        -------
        f:  float
            objective function value

        g:  list of booleans
            constraint expressions

        fail: integer
            pyOpt error flag to check if evaluation of f was succesfull
            or not
        """

        #index of parameter values at the end should be equal to the number
        #of free variables
        i = 0

        for key in model.free_parameters:
            model.parameters[key] = x[i]
            i += 1
        #check to see if everything went smooth

        if i != model.n_par:
            fmlog.warning("Free parameters at model level differ from"+
                            "parameters supplied by the objective function")


        #build the objective function

        fail = 0

        f = RSS(loadcase_dic, model)

        #build the constraints
        #equality and inequality type is handled in the problem definition
        #here we just need to convert the symbols into an expression
        #this is different from the OpenOpt wrapper

        if not model.constraints:
            #there are no constraints
            g = None
        else:
            g = [None]*len(model.constraints)
            j = 0
            for value in model.constraints.itervalues():
                i = 0
                constraint = value[0]
                for key in model.free_parameters.iterkeys():
                    constraint = constraint.replace(key,str(x[i]))
                    i += 1
                g[j] = eval(constraint)
                j += 1

        #set fail flag
        fail = 0
        #debug output
        fmlog.debug("Parameters")
        fmlog.debug(model.parameters)
        fmlog.debug("Residue: "+ str(f))

        return f, g, fail

    return obj




def set_solver(solver_options, solver_name ):
    """
    Creates a pyOpt solver instance and sets its options.
    Supported options are those specific to every solver as supported by
    pyOpt. Refer to pyOpt documentation at
    `pyOpt website <http://www.pyopt.org/index.html>`_
    for a list of available options

    Parameters
    ----------

    solver_options: dictionary
        dictionary with options (key=name, value=option value)

    solver_name:    string
        name of the solver

    Returns
    -------
    output: pyOpt solver instance
        a solver object with options set

    """
    #instantiate the solver
    output = pyOpt.__getattribute__(solver_name)()
    #set options
    for key, value in solver_options.iteritems():
        if key in output.options:
            #this long nested series of try statements has the purpose of
            #correctly specyfing the type of the option
            try:
                output.setOption(key,value)
            except IOError:
                try:
                    v = float(value)
                    output.setOption(key, v)
                except IOError or ValueError:
                    try:
                        v = int(value)
                        output.setOption(key, v)
                    except IOError or ValueError:
                        fmlog.error( "Solver Option " + key +
                                    "type incorrectly specified.\n"+
                                    "It must be " + output.options[key][0]+
                                    ".\n"+ "Using the default value"
                                    )
        else:
            fmlog.warning("for solver "+ solver_name + "there is no " + key +
                          "option.\nSkipping.")

    return output

def find_solution(solver_options, loadcase_dic, model, verbose):

    """
    Solves the optimization problem

    Parameters
    ----------

    solver_options: dictionary
        contains options according to scheme {`option name`: `value`}

    loadcase_dic: dictionary
        dictionary of loadcases cotaining instances of cases class

    model: material model object

    verbose: boolean
        flag for verbose output

    Returns
    -------

    model: model object
        updated versione of the model object with parameter values set equal
        to the optmization values

    result: dictionary

        a dictionary containing the following {`key`:`pair`} data

        `evals`:
            number of function evaluations
        `min`:
            objective function value corresponding to the minimum
        `parameters`:
            dictionary of parameters following the same scheme as
            the one used for the model objects
        `time`:
            execution time

        Restructuring the results is necessary to  have a single,
        solver-independent postprocessing routine

    """
    solver_name = FMglobs.FMsolver_name
    #define the objective fuction and optimization problem
    objective_function = obj_wrapper(loadcase_dic, model)
    #store the objective function in the model withour returning the constraints
    model.OBJ = lambda x: objective_function(x)[0]
    FMproblem = pyOpt.Optimization(model.NAME, objective_function)
    FMproblem.addObj(model.NAME)

    #defining variables and starting values
    for index, parameter in enumerate(model.free_parameters.keys()):
        FMproblem.addVar(parameter,
                        'c',
                        value = model.start[index],
                        lower = model.bounds[index][0],
                        upper = model.bounds[index][-1]
                         )
    #define constraints. Equality constraints are to be defined before
    #inequality constraints as per pyOpt docs so the need for the cycles

    for key in model.constraints:
        if model.constraints[key][1] == 'e':
            FMproblem.addCon(key,model.constraints[key][1])

    for key in model.constraints:
        if model.constraints[key][1] == 'i':
            FMproblem.addCon(key,model.constraints[key][1])

    #instantiate the solver class
    solver = set_solver(solver_options, solver_name)

    #solve the problem, we just need to get the minimum

    f_min = solver(FMproblem, disp_opts = True)[0]#[0]
    print FMproblem.solution(0)

    #restructure the results for the output
    result = {}
    result_position = {}
    min_position = FMproblem.solution(0).getVarSet()

    for item in min_position.itervalues():
        result_position[item.name] = item.value
        model.parameters[item.name] = item.value
        model.free_parameters[item.name] = item.value

    result['evals'] = FMproblem.solution(0).opt_evals

    result['min'] = f_min

    result['parameters'] = result_position

    result['time'] = FMproblem.solution(0).opt_time

    return model, result


