FMat version 0.1
--------------------

FMat is an optimisation program to identify material constants appearing in
material constitutive models that allows simultaneous fitting of data sets
coming from different types of experiments (loadcases in FMat) which can be
characterised by a different functional forms of the response function.

For instance identification of a hyperelastic model from biaxial and uniaxial
data requires to fit two set of data (strain, stress) to two different
functions expressing the stress in the two case.
Most of the data analysis software do not easily allow doing this.
FMat provides a simple framework to specify different loadcases that obey
different response functions that can be derived from a single material model.

Input to the program, which is written in Python, is provided by an input deck
which uses the ".ini" (Config Files) file syntax (GUIs are planned for future
versions, if I ever find the time to eventually learn QT and Curses).
The user can select material models from a library (that hopefully will grow in
the future) or provide a Python script to evaluate the model response for given
inputs at varying parameters.
FMat automatically assembles a single objective function from the different
loadcases.
To solve the resulting optimisation problem the user can select
solvers from the Python packages OpenOpt (http://openopt.org/) and pyOpt
(http://www.pyopt.org/).
You will need to install pyOpt in order to use FMat; since it is not available 
via `pip` you will have to follow the instructions on their site to install.
Requires a Fortran compiler.

Installing
----------
The software can be obtained via bitbucket.
To clone the git repository::
    
    git clone https://francesco_caimmi@bitbucket.org/francesco_caimmi/fmat.git

Move in the cloned directory and run (with administrative privileges if
required)::
    
    python setup.py install

See the `Python documentation <https://docs.python.org/2/install/>`_
and the setup script help (python setup.py install -h) for details on how to
customize the installation location.

Installation implies acceptance of the license terms and conditions which
can be found in the LICENSE.txt file. FMat is released under BSD license.

Running
--------

Up to now there is only a command line interface to FMat, which requires the
preparation of an input file. See the package documentation, which is included
in the installation directory for the details on how to prepare an input file.

The FMat executable, fmate.py, will be placed somewhere depending the
installation options you chose.
Once you have located it to use it just run::

    fmate.py <mode> -i <input file>

where <mode> is a mode specification such as batch or compare
and the -i switch specifies the input file.
Batch mode runs the input file in non interactive mode to perform the fitting
procedure.
Compare mode does not actually perform the fit but performs a comparison
between the data given in the "cases" section of the input file and the model
response specified by the the material parameters given by using the "init"
option in the "model" section of the input file.
Details on the operation modes can be found in the documentation.

To get help::
    
    fmate.py -h

The complete program call signature is::

    usage: fmate.py [-h] [-i INPUT] [-v VERSION] [-V] [-s] [-c CONFIG]
                    {compare,batch}

    FMat, material identification software

    positional arguments:
      {compare,batch}       Operation Mode

    optional arguments:
      -h, --help            show this help message and exit
      -i INPUT, --input INPUT
                            Input File
      -v VERSION, --version VERSION
                            Program Version
      -V, --verbose         Provide verbose output
      -s, --statistics      Perform statistic calculations on the model
      -c CONFIG, --config CONFIG
                            Specify an alternate configuration file

Citing
-------
If you used FMat for your work, please acknowledge the fact by citing it as::

    @Misc{,
      Title                    = {FMat 0.1 beta1 Material Identification Software},
      Author                   = {Francesco Caimmi},
      Year                     = {2016},
      Doi                      = {10.5281/zenodo.55735},
      Type                     = {Software},

      Owner                    = {fcaimmi},
      Timestamp                = {2016.06.16}
      }
