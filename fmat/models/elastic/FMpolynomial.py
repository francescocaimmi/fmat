# -*- coding: utf-8 -*-
"""
An implementation of the hyperelastic polynomial form of the strain energy
function with all the terms up order 2 (this follows ABAQUS implementation),
formulated for uncompressible materials.

The strain energy function is expressed as

.. math::

    U = \\sum_{i=1}^{n} \\sum_{j=1}^{m} C_{ij} (I_{1}-3)^i (I_{2}-3)^j,

where :math:`I_{1}` and :math:`I_{2}` are respectively the first and the second
invariant of the rigth ( or left) Chaucy-Green stretch tensor.
The present implementation
limits max{i,j} = 2 and the crossed terms contains at most :math:`C_{11}`

Model Definition and Usage
==========================

Name
----

The name of the material specified in the input file is ``elpol``


**Input file usage**: ``name = elpol``
Material Parameters
-------------------


Material parameters belonging to the model are then

1. `c10`: first invariant,order one coefficient
2. `c01`: second invariant,order one coefficient
3. `c20`: first invariant,order two coefficient
4. `c11`: mixed order two coefficient
5. `c02`: second invariant,order two coefficient


Response Functions
------------------

The response functions available are

1.uniaxial: key = uniaxial

2.pure shear: key = pure shear

3.Equibiaxial: key = biaxial




**Input file usage**:
                    under the heading ``[LoadCasen]`` specify
                    ``t_type = member of the list``

See the Module documentation for details on how to specify the input data
for the various load cases.

Module documentation
======================

"""

#Python imports
import numpy as np

try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

#FMat imports

from fmat.models.FMmodels import Model
from fmat.models.FMmodels import warn_for_data


#response functions

def pl_uniaxial(loadcase, model):
    """
    Routine Listing
    ----------------

    uniaxial response function

    Parameters
    ----------

    loadcase : loadcase object
        loadcase x data are stretches (numpy array) and only
        one set of data is recognized and used

    model : model object

    Returns
    -------

    stress: (numpy array)
        vector of uniaxial stresses

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail (unused at the moment)

    """
    model.fail = 0
    lambda1 = loadcase.x[0][0]

    c10 = model.parameters['c10']
    c01 = model.parameters['c01']
    c20 = model.parameters['c20']
    c11 = model.parameters['c11']
    c02 = model.parameters['c02']

    I1 = np.power(lambda1,2) + 2*np.divide(1,lambda1)
    I2 = np.power(lambda1,-2) + 2*lambda1

    stress = 2*(1-np.power(lambda1,-3))*\
            (c10*lambda1 + c01 +2*c20*lambda1*(I1-3)+\
             c11*(I1 - 3 + lambda1*(I2 - 3)) + 2*c02*(I2 - 3)\
                )


    return stress

def pl_ps(loadcase, model):
    """
    Routine Listing
    ----------------

    pureshear response function

    Parameters
    ----------

    loadcase : loadcase object
        loadcase x data are pure shear stretches (numpy array) and only
        one set of data is recognized and used

    model : model object

    Returns
    -------

    stress: (numpy array)
        vector of pure shear stresses

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail (unused at the moment)

    """
    model.fail = 0
    lambda1 = loadcase.x[0][0]

    c10 = model.parameters['c10']
    c01 = model.parameters['c01']
    c20 = model.parameters['c20']
    c11 = model.parameters['c11']
    c02 = model.parameters['c02']

    I1 = np.power(lambda1,2) + np.power(lambda1,-2) + 1

    stress = 2*(lambda1-np.power(lambda1,-3))*\
            (c10 + c01 + 2*(c20 + c11 + c02)*(I1 - 3)\
                )

    return stress

def pl_equibia(loadcase, model):
    """
    Routine Listing
    ----------------

    biaxial response function

    Parameters
    ----------

    loadcase : loadcase object
        loadcase x data are biaxial stretches (numpy array) and only
        one set of data is recognized and used

    model : model object

    Returns
    -------

    stress: (numpy array)
        vector of pure shear stresses

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail (unused at the moment)

    """
    model.fail = 0
    lambda1 = loadcase.x[0][0]

    c10 = model.parameters['c10']
    c01 = model.parameters['c01']
    c20 = model.parameters['c20']
    c11 = model.parameters['c11']
    c02 = model.parameters['c02']

    I1 = 2*np.power(lambda1,2) + np.power(lambda1,-4)
    I2 = 2*np.power(lambda1,-2) + np.power(lambda1,4)

    stress = 2*(lambda1-np.power(lambda1,-5))*\
            (c10 + c01*np.power(lambda1,2) + 2*c20*(I1 - 3)+\
             c11*(np.power(lambda1,2)*(I1 - 3) + (I2 - 3))+\
             2*c02*np.power(lambda1,2)*(I2 - 3)
                )


    return stress



###########
#class
###########
class elpol(Model):
    """
    See :ref:`model_attributes` for parameters common to all material
    models. Here only additional parameters will be listed.

    Parameters
    ----------

    as in parent class

    Returns
    -------

    as in parent class


    Attributes
    -----------------

    The same as the parent model class


    Other Parameters
    --------------------

    Material Parameters available are as listed in the constant
    `PARAMETER_NAMES`:
    `c10`: first invariant, order one coefficient
    `c01`: second invariant, order one coefficient
    `c20`: first invariant, order two coefficient
    `c11`: mixed, order two coefficient
    `c02`: second invariant, order two coefficient

    Methods
    -------

    None in addition to parent class (see :ref:`model_attributes`)
    """

    PARAMETER_NAMES = ['c10','c01','c20','c11','c02']

    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC = {'uniaxial':pl_uniaxial,
                'pure shear': pl_ps,
                'biaxial': pl_equibia}

    NAME = 'elpol'

