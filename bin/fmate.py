#!/usr/bin/python
# -*- coding: UTF-8 -*-
__version__ ='0.1'

"""


Author Information
Francesco Caimmi, Ph.D.
Laboratorio di Ingegneria dei Polimeri
Politecnico di Milano - Dipartimento di Chimica, Materiali e Ingegneria
Chimica
"Giulio Natta"

P.zza Leonardo da Vinci, 32
I-20133 Milano
Tel. +39.02.2399.4711
Fax +39.02.7063.8173

francesco.caimmi@polimi.it
Skype: fmglcaimmi
GPG Public Key : http://goo.gl/64dDo

FMat is a program for material parameters identification through inverse
analysis. It must be feed with eperimental data and it will provide
material parameters. It may use both built-in material models and
interfaces to Finite Elements codes in order to minimize the difference between
the prediction from the codes and the data.

General Compatibility
-Python 2.6 (limited testing)
-Python 2.7 (developed on)


FE Codes Interfaces
-ABAQUS 6.11

Dependencies
==============

For the general use

    -Numpy
    -Scipy > 0.9
    -Matplotlib
    -pyOpt
    -ordereddict for Python older than 2.6

Additional supported modules

    -OpenOpt



ABAQUS Interface Dependencies

   -ABAQUS Python modules


"""
#******************************************************************************
#Version 0.1
#Initial implementation
#******************************************************************************
#fmate.py is the progam executable
#******************************************************************************
#This file was first created on Mar 11, 2011 16:06
#******************************************************************************
#*****************PROGRAM BEGINS HERE******************************************

#standard library and standard dependencies module import
import os
import sys
import argparse
import ConfigParser
import logging



#"global" program variables

fm_path_s = os.path.normcase('/')#platform independent path separator



#imports program moduli and libraries
#from FMLib import FMconfig #FMconfig is fMat configuration reader module
from fmat.FMLib import FMglobs
from fmat.FMLib.RSS import RSS
from fmat.FMLib import FMlogger


#******************BEGIN MAIN*****************
def main(mode,
         inputfile = None,
         config = "FMatConfig.cfg",
         statistics = False,
         verbose = False,
         *FMArgs,**FMkeywords):

    """
    fmat.main() is the main module for the program.

    Parameters
    -----------

    inputfile: string
        filename to read in order to start the analysis. Specified via
        Config Parser syntax

    config: string
        configuration file

    statistics: boolean
        whether or not to perform a statistical analysis of the fit.
        Defualts to False

    verbose: boolean
        flag to trigger verbose output
        Defualts to False

    """

    if verbose == True:
        FMglobs.FMverbose = True
    if statistics == True:
        FMglobs.FMstatistics = True
    #initialize the logger object to access logging facilities
    FMlogger.initLogging(verbose)
    fmlog = logging.getLogger(FMglobs.FMLOGGER)

    #command line interface invokation
    if mode in ['batch','compare']:

        solution = {}

        #import of batch functions
        from fmat.FMLib import FMbatch

        #initial input processing
        fmlog.info('Begin input file processing')
        fminput = ConfigParser.ConfigParser()

        #preserve case for options
        fminput.optionxform = str
        fminput.read(inputfile)
        #input file debug output
        fmlog.debug("Sections in input file")
        fmlog.debug('\n'.join(fminput.sections()))
        #set the solver name
        FMbatch.set_solver_name(fminput)

        #Load Cases dictionary construction

        dic_LoadCases, n_LoadCases = FMbatch.build_cases(fminput, verbose)

        #Material Model Object construction
        mat_model, n_parameters = FMbatch.build_model(fminput,
                                                      dic_LoadCases,
                                                      verbose)
        fmlog.info('End input file processing')
        #if batch processing is requested, solve the optimization problem
        #otherwise continue to the output using the input parameter
        if mode == 'batch':
            mat_model, solution = FMbatch.solve(fminput, dic_LoadCases,
                                   mat_model, verbose)
            fmlog.info( "Check "+ 
                       FMglobs.FMsolver_name +
                       ".out for output parameters")
            fmlog.info("Residual Sum of Squares:" + str(solution['min']))

        else :

            #set model parameters equal to starting values, exploit the
            #fact the dictionary is ordered
            index = 0

            for key in mat_model.free_parameters.iterkeys():
                mat_model.parameters[key] = mat_model.start[index]

                index += 1
                #assign the parameters to the free parameters dict too
            for key in mat_model.free_parameters:
                mat_model.free_parameters[key] = mat_model.parameters[key]
            #if verbose output was chosen, print the material parameters
            fmlog.debug('Model Parameters')
            fmlog.debug(mat_model.parameters)

            #evaluate the residual
            residue = RSS(dic_LoadCases, mat_model)
            fmlog.info("Residual Sum of Squares: "+str(residue))

        #prepare the graphs and text output
        FMbatch.model_output(dic_LoadCases, mat_model)
        #if requested perform statistical outputs
        if FMglobs.FMstatistics:
            FMbatch.stat_output(dic_LoadCases, mat_model)

    #graphical interfaces invocation

    elif mode == 'GUI':
        fmlog.error('GUI mode is not supported in this version')
        sys.exit(1)

    elif mode == 'CLI':
        fmlog.info('CLI support is very limited in this version')
        from fmat import FMcurses
        FMcurses.FMcurses.main()

    return dic_LoadCases,mat_model,solution

#******************END MAIN*****************


#call
if __name__ == "__main__":
    #build the command line argument parser
    fmparser = argparse.ArgumentParser(description=
                                    'FMat, material identification software')
    fmparser.add_argument('mode',help = "Operation Mode",
                          choices=FMglobs.OPERATION_MODE_DICTIONARY.keys() )
    fmparser.add_argument('-i','--input',help = "Input File",default= None )
    fmparser.add_argument('-v','--version',help = "Program Version" )
    fmparser.add_argument('-V','--verbose',help = "Provide verbose output",
                          action='store_true', default=False
                          )
    fmparser.add_argument('-s','--statistics',
                          help = "Perform statistic calculations on the model",
                          action='store_true', default=False
                          )
    fmparser.add_argument('-c','--config',help =
                        "Specify an alternate configuration file" )
    #parse commandline arguments

    fmoptions = vars(fmparser.parse_args(sys.argv[1:]))
    
    #check if input file is specified for batch processing
    if fmoptions['input'] == None and fmoptions['mode'] == 'batch':
        fmlog.error("Input file is required in batch mode\nExit")
        sys.exit(1)
    
    #let's dance
    print ("FMat Material Parameters Identification software. Version:", \
            __version__)
    a,b,c=main(
        fmoptions['mode'],
        inputfile=fmoptions['input'],
        config = fmoptions['config'],
        statistics = fmoptions['statistics'],
        verbose = fmoptions['verbose']
        )

#*****************PROGRAM ENDS HERE*******************************************
