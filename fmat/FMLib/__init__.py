"""
FMLib provides various modules and utilities for the execution of jobs, the
configuration of FMat, the analysis and the visualization of the results.


"""
import FMconfig
import FMbatch
import FMglobs
import FMplot
import RSS
__all__=["FMconfig","FMbatch","FMglobs","FMplot","RSS"] 
