# -*- coding: utf-8 -*-
"""
Class for using the Ogden Incompressible model implemented in ABAQUS (C)

Model Definition and Usage
==========================

Name
----

The name of the material specified in the input file is ``ABAQUS_ogden``

**Input file usage**: ``name = ABAQUS_ogden``

Material Parameters
-------------------

Material parameters belonging to the model are

1. `alpha_i:` exponents of the Ogden Model
2.  `mu_i`: moduli of the Ogden model
3.  `D_i`:    moduli for the volumetric part
        Set to zero in order to use incompressible Ogden

there are as many couples as n (`i.e.` `i` in range [1..`n`])

Response Functions
------------------
The response functions available are

1.generic:  key = generic
        generic response function

In addition to the response function used with ABAQUS the response
function for the :ref:`FMogden` material model are also provided in order
to use in conjuction with results from simple experiments. This will work
only for uncompressible material. If a compressible Ogden model were included
in FMat, the functionality will be extended.



**Input file usage**:
                    under the heading ``[LoadCasen]`` specify
                    ``t_type = key``

See the Module documentation for details on how to specify the input data
for the various load cases.

Module documentation
======================
"""

#Python imports
try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict
import sys
import numpy as np
#import pdb

#Internal imports
from fmat.models.FMmodels import Model
from fmat.models.ABAQUS.FMabq_lib import run_analysis
from fmat.models.ABAQUS.FMabq_lib import run_postprocessing
from fmat.models.ABAQUS.FMabq_lib import input_file_names
from fmat.models.ABAQUS.FMabq_lib import manage_optional_dictionaries

###########
#CONSTANTS
###########

#these are the lines to be written in the ABAQUS input file as a function
#of the number of model terms `n`

__INPUT_FILE_LINES ={
    '1':'<mu_1>,<alpha_1>,<D_1>\n',

    '2':'<mu_1>,<alpha_1>,<mu_2>,<alpha_2>,<D_1>,<D_2>\n',

    '3':'<mu_1>,<alpha_1>,<mu_2>,<alpha_2>,<mu_3>,<alpha_3>,<D_1>,<D_2>\n<D_3>\n',

    '4':'<mu_1>,<alpha_1>,<mu_2>,<alpha_2>,<mu_3>,<alpha_3>,<mu_4>,<alpha_4>\n\
        <D_1>,<D_2>,<D_3>,<D_4>\n',

    '5':'<mu_1>,<alpha_1>,<mu_2>,<alpha_2>,<mu_3>,<alpha_3>,<mu_4>,<alpha_4>\n\
        <mu_5>,<alpha_5>,<D_1>,<D_2>,<D_3>,<D_4>,<D_5>\n',

    '6':'<mu_1>,<alpha_1>,<mu_2>,<alpha_2>,<mu_3>,<alpha_3>,<mu_4>,<alpha_4>\n\
        <mu_5>,<alpha_5>,<mu_6>,<alpha_6>,<D_1>,<D_2>,<D_3>,<D_4>\n\
        <D_5>,<D_6>\n'
}

#similarly this is the number of lines to skip while modyfing the input file
#after writing the material parameter line

__INPUT_FILE_SKIP={
    '1':1, '2':1, '3':1, '4':2, '5':2, '6':3
}

####################
#Response Functions
####################
def generic(loadcase, model):
    """
    Routine Listing
    ----------------

    Generic response function

    Parameters
    ----------
    loadcase:  loadcase object
        loadcase data are generic data consistent with what happens
        in the user supplied postprocessing script

    model:  model object

    Returns
    -------
    return: numpy array
        generic output as defined by model.post_script (numpy array)

    Response functions for uniaxial,pure shear and biaxial cases.
    Cannot be imported by the corresponding elastic Ogden module as the
    conventions used by ABAQUS for the parameters are different; see ABAQUS
    manual.
    These functions work only for the incompressible case.
    """
    error_flag = False
    model.fail = 0
    #check if we have an input file
    filename, orname = input_file_names(loadcase, model)

    if filename != None:


        abq_input_file = open(filename, 'w')

        #copy the parameters to the input file

        with open(orname,'r') as old_input:
            for line in old_input:
                #prepare parameters near the heading file
                if '*Heading' in line:
                    abq_input_file.write(line)
                    abq_input_file.write('*PARAMETER\n')
                    for key in model.parameters:
                        abq_input_file.write(key+' = '
                                            +str(model.parameters[key])+'\n')
                #write the parameters
                #abaqus convention is mu_i,alpha_i all the D_i at the end
                elif '*Hyperelastic' and 'ogden' in line:
                    abq_input_file.write(line)
                    abq_input_file.write(__INPUT_FILE_LINES[str(model.n)])
                    #skip the appropriate number of lines
                    i = 0
                    while i <=__INPUT_FILE_SKIP[str(model.n)]:
                        line=next(old_input)
                        i += 1
                else:
                    abq_input_file.write(line)

        abq_input_file.close()
        #run_analysis()


        error_flag = run_analysis(job_name = filename.split('.inp')[0] ,
                                  cmd_options = model.abaqus_options)
        #pdb.set_trace()
        #run_postprocessing(loadcase, model)

        error_flag, result = run_postprocessing(loadcase, model)
    #we do not have an input file; assume the post-processing script will
    #do all the job
    else:
        error_flag, result = run_postprocessing(loadcase, model)

    if error_flag == True: #something went wrong
        model.fail = 1
        return np.ones(len(loadcase.x[0]))

    return result

# Response functions for uniaxial,pure shear and biaxial cases.
# Cannot be imported by the corresponding elastic Ogden module as the
# conventions used by ABAQUS for the parameters are different
def ogden_PK1_uniaxial(loadcase, model):
    """


    uniaxial response function

    Parameters
    ----------
    loadcase : loadcase object
        loadcase x data are stretches (numpy array) and only
        one set of data is recognized and used

    model : model object

    Returns
    -------
    First component of the Piola-Kirchhoff stress tensor: numpy array

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail
    """
    model.fail = 0
    n = model.n_par/2
    lambda1 = loadcase.x[0][0]
    param = [model.parameters['alpha_'+str(i+1)] for i in range(n)]+\
            [model.parameters['mu_'+str(i+1)] for i in range(n)]
    return sum(
            0.5*param[k+n]*param[k]*\
            (lambda1**(param[k]-1)-lambda1**(-0.5*param[k]-1))
            for k in range(n)
            )


def ogden_PK1_biaxial(loadcase, model):
    """
    equi-biaxial response function

    Parameters
    ----------

    loadcase : loadcase object
                loadcase x data are stretches (numpy array) and only
                one set of data is recognized and used
    model : model object

    Returns
    -------

    First component of the Piola-Kirchhoff stress tensor: numpy array

    fmodel.fail:    (int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail
    """
    model.fail = 0
    n = model.n_par/2
    lambda1 = loadcase.x[0][0]
    param = [model.parameters['alpha_'+str(i+1)] for i in range(n)]+\
            [model.parameters['mu_'+str(i+1)] for i in range(n)]

    return sum(
                0.5*param[k+n]*param[k]*\
                (lambda1**(param[k]-1)-lambda1**(-2*param[k]-1))
                for k in range(n)
                )


def ogden_PK1_ps(loadcase, model):
    """
    pure shear response function

    Parameters
    ----------
    loadcase : loadcase object
                loadcase x data are stretches (numpy array) and only
                one set of data is recognized and used
    model : model object

    Returns
    -------
    First component of the Piola-Kirchhoff stress tensor: numpy array

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail
    """
    model.fail = 0
    n = model.n_par/2
    lambda1 = loadcase.x[0][0]
    param = [model.parameters['alpha_'+str(i+1)] for i in range(n)]+\
            [model.parameters['mu_'+str(i+1)] for i in range(n)]

    return sum(
                0.5*param[k+n]*param[k]*\
                (lambda1**(param[k]-1)-lambda1**(-1*param[k]-1))
                for k in range(n)
                )

class ABAQUS_ogden(Model):
    """
    Class for implementing ABAQUS(C) Ogden Model.
    The class work for both compressible and incompressible materials. For an
    incompressible material just fix the parameters D_i to zero.


    The reader is referred to ABAQUS manual for details on the parameters.

    Parameters
    ----------

    as in parent class

    Returns
    -------

    as in parent class


    Attributes
    -----------------

    n: integer, mandatory
        number of terms in Ogden Model. Must be specified input file

    In addition to Model attributes and ABAQUS model common options as
    described in the models.ABAQUS documentation, ABAQUS_ogden instances
    also have the following members

    Additional Parameters
    ----------------------

    alpha_i:
        exponents of the Ogden Model
    mu_i:
        moduli of the Ogden model
    D_i:
        moduli for the volumetric part


    Methods
    -------


    """
    #abaqus allows for a maximum of 6 elements in the model

    MATERIAL_PARAMETERS = ['alpha_'+str(i+1) for i in range(6)]+\
                        ['mu_'+str(i+1) for i in range(6)]+\
                        ['D_'+str(i+1) for i in range(6)]

    MODEL_MANDATORY = ['post_script', 'n']

    MODEL_OPTIONS =['input_file', 'abaqus_options', 'post_output']

    PARAMETER_NAMES = MATERIAL_PARAMETERS + MODEL_MANDATORY + MODEL_OPTIONS


    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC={'generic':generic,
              'uniaxial':ogden_PK1_uniaxial,
              'biaxial': ogden_PK1_biaxial,
              'pure shear': ogden_PK1_ps,
              }

    NAME = 'ABAQUS_ogden'

    def set_parameters(self, fixed_parameters):
        """
        Parameters
        ----------

        fixed_parameters: dictionary
            a dictionary containing keys correspondi to material
            parameters  and their fixed value

        Returns
        -------

        self.fixed_parameters, self.free_parameters, self.parameters:
            dictionaries
            dictionaries containing the free material parameters,
            the fixed ones and the union of the two
        """
        try:
            n = int(fixed_parameters['n'])
        except KeyError:
            print "ERROR: Number of elements in Ogden model (n) not specified"
            print "Quitting"
            sys.exit(333)

        #creates parameter dictionary
        parameters = OrderedDict.fromkeys(self.PARAMETER_NAMES, None)

        #set fixed parameters and option
        for key in fixed_parameters.keys():
            parameters[key] = fixed_parameters[key]


        #some parameters that must be specified are actually options and not
        #material parameters; me must therefore remove them and store the
        #information elsewhere this is done twice for mandatory and optional
        #model parameters
        for key in self.MODEL_MANDATORY:

            try:
                value = parameters.pop(key)
                if key == 'post_script':
                    manage_optional_dictionaries(key, value, self)
                else:
                    setattr(self, key, value)
                fixed_parameters.pop(key)

            except KeyError:
                print "ERROR: unspecified ", key, "for the material model"
                sys.exit(666)

        for key in self.MODEL_OPTIONS:
            try:
                value = parameters.pop(key)
                #lets treat the input file case
                if key == 'input_file':
                    manage_optional_dictionaries(key, value, self)
                #here we instead treat the case of the post_output files
                elif key == 'post_output':
                    manage_optional_dictionaries(key, value, self)
                else:
                    setattr(self, key, value)
                fixed_parameters.pop(key)
            except KeyError:
                setattr(self, key, None)


        self.PARAMETER_NAMES = ['alpha_'+str(i+1) for i in range(n)]+\
                        ['mu_'+str(i+1) for i in range(n)]+\
                        ['D_'+str(i+1) for i in range(n)]

        self.MAX_PARAM = len(self.PARAMETER_NAMES)

        #next fixed parameters must handled
        free_parameters = OrderedDict.fromkeys(self.PARAMETER_NAMES, None)

        for key in fixed_parameters:
            free_parameters.pop(key)

        #try to convert the parameters if this is the case
            try:
                fixed_parameters[key] = float(fixed_parameters[key])
            except ValueError:
                pass


        self.free_parameters = free_parameters
        self.fixed_parameters = fixed_parameters
        self.parameters = OrderedDict(  self.free_parameters.items() + \
                                        self.fixed_parameters.items() )

        n_free = len(self.free_parameters)

        if n_free <= 0:
            print "ERROR: no free parameters left for identification"
            sys.exit(666)

        self.n_par = n_free