fmat.models.viscoelastic package
================================

.. automodule:: fmat.models.viscoelastic
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

fmat.models.viscoelastic.FMBB module
------------------------------------

.. automodule:: fmat.models.viscoelastic.FMBB
    :members:
    :undoc-members:
    :show-inheritance:

fmat.models.viscoelastic.FMprony module
---------------------------------------

.. automodule:: fmat.models.viscoelastic.FMprony
    :members:
    :undoc-members:
    :show-inheritance:


