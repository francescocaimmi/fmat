# -*- coding: utf-8 -*-
#author: Francesco Caimmi francesco dot caimmi @ polimi dot it
#see LICENSE.txt in the root source directory for licensing information
from setuptools import setup
import os

#for now the package list is manageble by hand
#from setuptools import find_packages

#TODO finish this stuff to build the documentation contestually with
#the project seutp
#create the list of documentation files to be included with the project
#base direcotry for html sphinx generated documentation
BASE_SCR_DOC_DIR = 'Documentation/sphinx/_build/html/'
#base installation directory for documentation
BASE_PKG_DOC_DIR = 'docs'
pkg_doc_files = []
for node in os.walk(BASE_SCR_DOC_DIR):

    files = []
    if node[0] == BASE_SCR_DOC_DIR:
        directory = BASE_PKG_DOC_DIR
        for f in node[2]:
            files.append(os.path.join(node[0],f))
        pkg_doc_files.append((directory,files))

    else:
        directory = os.path.join(BASE_PKG_DOC_DIR,
                                 node[0].split(BASE_SCR_DOC_DIR)[-1])
        for f in node[2]:
            files.append(os.path.join(node[0],f))
        pkg_doc_files.append((directory,files))


setup(name='FMat',
      version = '0.1b1',
      description ='FMat material identification software',
      author = 'Francesco Caimmi',
      author_email = 'francesco.caimmi@polimi.it',
      url = 'https://bitbucket.org/francesco_caimmi/fmat',
      license='BSD',
# in case some day we move to an automated procedure
#      packages = find_packages(exclude=('templates',
#                                        'Documentation')),
      packages = ['fmat',
                      'fmat.FMcurses',
                      'fmat.FMLib',
                      'fmat.solvers',
                      'fmat.models',
                          'fmat.models.ABAQUS','fmat.models.elastic',
                          'fmat.models.viscoelastic'],
      install_requires = ['pyopt>=1.0',
                          'numpy>=1.5',
                          'scipy>=0.9',
                          'swig',
                          'numdifftools'],
      package_data={'fmat': ['test_cases/*'], #test for the users
      },
      zip_safe=True,
      scripts=['bin/fmate.py'],
#      entry_points = {
#        'console_scripts': ['fmate=bin.fmate:main'],},
      #data files is a lits of tuples [(dir,[files])]
      data_files= pkg_doc_files + #documentation
      [('fmat',['fmat/FMatConfig.cfg'])#configuration file
                  ]
     )
