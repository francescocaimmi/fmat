fmat.FMcurses package
=====================

.. automodule:: fmat.FMcurses
    :members:
    :undoc-members:
    :show-inheritance:

Submodules
----------

fmat.FMcurses.FMcurses module
-----------------------------

.. automodule:: fmat.FMcurses.FMcurses
    :members:
    :undoc-members:
    :show-inheritance:


