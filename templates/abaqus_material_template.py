# -*- coding: utf-8 -*-
"""
Class for using Ogden Incompressible model in ABAQUS (C)
"""

#Python imports
try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

#Internal imports
from models.FMmodels import Model
from models.ABAQUS.FMabq_lib import run_analysis
from models.ABAQUS.FMabq_lib import run_postprocessing
from models.ABAQUS.FMabq_lib import input_file_names

####################
#Response Functions
####################
def generic(loadcase, model):
	"""
    Generic response function
    
    Input
    -------
    *loadcase*:  loadcase object
        loadcase data are generic data consistent with what appens
        in the user supplied postprocessing script
        
    *model* :
        model object
    
    Output
    -------
     
    generic output as defined by model.post_script (numpy array)
    """

    
class ABAQUS_somewhat(Model):
    """
    Class for implementing ABAQUS(C) somewhat Model.

    
    
    The reader is referred to ABAQUS manual for details on the parameters.
    
    ----------    
    Parameters
    ----------
    
        
    ----------
    Attributes
    ----------
    
    In addition to Model attributes and ABAQUS model common options as 
    described in the models.ABAQUS documentation, ABAQUS_ogden instances 
    also have the following members
    
    
    -------        
    Methods
    -------
    In addition to parent class Model methods, the following methods are 
    available or redifined 

    """
    PARAMETER_NAMES = []
    
    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC = dict()    
    
    NAME = ''