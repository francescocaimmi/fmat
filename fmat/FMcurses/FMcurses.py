# -*- coding: utf-8 -*-
"""
Initial implementation of curses interface

routine listing
---------------
"""
#Python imports
import curses


def main():
    """
    Main module for the curses interface
    
    Parameters
    ----------
    
    Returns
    --------
    """
    #init screen
    FMscreen = curses.initscr()
    #set curses options
    curses.noecho()
    curses.curs_set(0)

    FMscreen.keypad(1)
    #loadcases windows
    begin_x = 0 
    begin_y = 0
    height = 25 
    width = 25
    cases_win = curses.newwin(height, width, begin_y, begin_x)
    cases_win.border(0)

    cases_win.addstr("LOADCASES", curses.A_BOLD)
    cases_win.refresh()
    FMscreen.refresh()
    
    #draw the main app
    FMscreen.addstr("This is a String. I told you support was limited...")
    FMscreen.addstr("q to quit")
    
    FMscreen.refresh()
    while True:
        event = FMscreen.getch()
        if event == ord("q"): break
    #ends     
    curses.endwin()
    