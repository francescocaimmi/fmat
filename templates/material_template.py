# -*- coding: utf-8 -*-
"""
Description of the model

Model Definition and Usage
==========================

Name
----

The name of the material specified in the input file is ```` 

**Input file usage**: ``name = ``

Material Parameters
-------------------


Material parameters belonging to the model are then

1. `par`: description



Response Functions
------------------
The response functions available are 

1.res:  key = input key



**Input file usage**: 
                    under the heading ``[LoadCasen]`` specify
                    ``t_type = member of the list``

See the Module documentation for details on how to specify the input data
for the various load cases.

Module documentation
======================

"""

#Python imports

#FMat imports

from models.FMmodels import Model
from models.FMmodels import warn_for_data

try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict


#response functions

def res_func(loadcase, model):
    """
    Routine Listing
    ----------------
    
    response function description   
    
    Parameters
    ----------
    loadcase : loadcase object
        loadcase x data are stretches (numpy array) and only 
        one set of data is recognized and used
    
    model : model object
    
    Returns
    -------
    result: (numpy array) 
    
    model.fail:(int)
        flag specifing if the evaluation was succesfull 
        O for success, 1 for fail
    """
    model.fail = 0
    




###########
#class
###########
class material(Model):
    """
    See :ref:`model_attributes` for parameters common to all material
    models. Here only additional parameters will be listed.
    
    Parameters
    ----------
    
    as in parent class 
    
    Returns
    -------
    
    as in parent class 
    
    
    Attributes
    -----------------
    
    The same as the parent model class 
    
    
    Other Parameters
    --------------------

    Material Parameters available are as listed in the constant 
    `PARAMETER_NAMES`
    
    `E`: elastic modulus
    `nu`: Poisson's coefficient    
    
    Methods
    -------
    
    None in addition to parent class (see :ref:`model_attributes`)
    """
    
    PARAMETER_NAMES = []
    
    MAX_PARAM = len(PARAMETER_NAMES)
    
    RES_FUNC = { }
    
    NAME = ''
    