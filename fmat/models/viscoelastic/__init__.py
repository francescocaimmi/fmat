# -*- coding: utf-8 -*-
"""
Viscoelastic Material models for FMat

"""
from FMprony import Prony
__all__ = ["FMprony"]