# -*- coding: utf-8 -*-
#see LICENSE.txt in the root source directory for licensing information
"""
Hooke elastic material implentation for FMat

Model Definition and Usage
==========================

Name
----

The name of the material specified in the input file is ``Hooke``

**Input file usage**: ``name = Hooke``

Material Parameters
-------------------

Material parameters belonging to the model are

1. `E`: Young's modulus

2. `nu`: Poisson's coefficient

Response Functions
------------------
The response functions available are

1.uniaxial

2.poisson


**Input file usage**:
                    under the heading ``[LoadCasen]`` specify
                    ``t_type = uniaxial|poisson``

See the Module documentation for details on how to specify the input data
for the various load cases.

Module documentation
======================


"""

from fmat.models.FMmodels import Model
from fmat.models.FMmodels import warn_for_data

####################
#response functions
###################
def hooke_uniaxial(loadcase,model):
    """

    uniaxial response function taking strains and returning stress


    Parameters
    ----------


    loadcase : loadcase object
        loadcase X-data are strains (numpy array) and only one
        set of strains is used; if more are present, the are disregarded
        the assumed shape of the X-data array is `1xjx1` where j is the number
        of measured strains

    parameters : dictionary
        material parameters


    Returns
    -------

    stress: numpy array
        strain in the other direction

    """
    #check to see that there are no more than 1 x value
    warn_for_data(1, loadcase)
    #do the calculations
    x = loadcase.x[0]
    E = model.parameters['E']
    model.fail = 0
    return E * x[0]

def hooke_poisson(loadcase,model):
    """
    poisson response function taking strain in one direction
    and returning strain in transverse direction

    Parameters
    ----------


    loadcase : loadcase object
        loadcase X-data are strains (numpy array) and only one
        set of strains is used; if more are present, the are disregarded
        the assumed shape of the X-data array is `1xjx1` where j is the number
        of measured strains

    parameters : dictionary
        material parameters


    Returns
    -------

    strain: numpy array
        strain in the other direction

    """
    #check to see that there are no more than 1 x value
    warn_for_data(1, loadcase)
    #do the calculations
    x = loadcase.x[0]
    nu= model.parameters['nu']
    model.fail = 0
    return -nu * x[0]

####################
#class
###################
class Hooke(Model):
    """
    Class that implements linear elastic materials

    See :ref:`model_attributes` for parameters common to all material
    models. Here only additional parameters will be listed.

    Parameters
    ----------

    as in parent class

    Returns
    -------

    as in parent class


    Attributes
    -----------------

    The same as the parent model class


    Other Parameters
    --------------------

    Material Parameters available are as listed in the constant
    `PARAMETER_NAMES`

    `E`: elastic modulus
    `nu`: Poisson's coefficient

    Methods
    -------

    None in addition to parent class (see :ref:`model_attributes`)
    """

    PARAMETER_NAMES = ['E','nu']

    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC = {'uniaxial':hooke_uniaxial,
                'poisson': hooke_poisson,
                       }

    NAME = 'Hooke'
