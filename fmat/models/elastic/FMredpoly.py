# -*- coding: utf-8 -*-
"""
.. py:module:: FMredpoly:

Implementation of the reduced polynomial model for FMat, formulated for
uncompressible materials.

The strain energy function is expressed as

.. math::

    U = \\sum_{i=1}^{n} C_{i} (I_{1}-3)^i

where :math:`I_{1}` is the first invariant of the rigth ( or left)
Chaucy-Green stretch tensor.

Model Definition and Usage
==========================

Name
----

The name of the material specified in the input file is ``rpol``

**Input file usage**: ``name = rpol``

Material Parameters
-------------------

To use Ogden material the number `n` of elements in the model (term in the
summation) **must** be specified

**Input file usage**: ``n = number``

Material parameters belonging to the model are then

1. `c_i`: moduli



there are as many `c_i` as n (`i.e.` `i` in range [1..`n`])

Response Functions
------------------
The response functions available are

1.uniaxial: key = uniaxial

2.pure shear: key = pure shear

3.Equibiaxial: key = biaxial



**Input file usage**:
                    under the heading ``[LoadCasen]`` specify
                    ``t_type = key``


See the Module documentation for details on how to specify the input data
for the various load cases.

Module documentation
======================

"""

#Python imports
import logging
import sys
import numpy as np

#FMat imports

from fmat.models.FMmodels import Model
from fmat.models.FMmodels import warn_for_data
from fmat.FMLib import FMglobs

try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

#prepare the logger
fmlog = logging.getLogger(FMglobs.FMLOGGER)


#response functions

def rp_PK1_uniaxial(loadcase, model):
    """
    Routine Listing
    ----------------

    uniaxial response function

    Parameters
    ----------
    loadcase : loadcase object
        loadcase x data are stretches (numpy array) and only
        one set of data is recognized and used

    model : model object

    Returns
    -------
    First component of the Piola-Kirchhoff stress tensor: numpy array

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail
    """
    model.fail = 0
    n = model.n_par
    lambda1 = loadcase.x[0][0]
    param = [model.parameters['c_'+str(i+1)] for i in range(n)]
    I1 = np.power(lambda1,2) + 2*np.divide(1,lambda1)
    #the index in the sum cannot start from zero!
    return 2*(lambda1-lambda1**(-2))*\
            sum(
            param[k]*(k+1)*(I1 - 3)**(k+1-1)
            for k in range(n)
            )


def rp_PK1_biaxial(loadcase, model):
    """
    equi-biaxial response function

    Parameters
    ----------

    loadcase : loadcase object
                loadcase x data are stretches (numpy array) and only
                one set of data is recognized and used
    model : model object

    Returns
    -------

    First component of the Piola-Kirchhoff stress tensor: numpy array

    fmodel.fail:    (int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail
    """
    model.fail = 0
    n = model.n_par
    lambda1 = loadcase.x[0][0]
    param = [model.parameters['c_'+str(i+1)] for i in range(n)]
    I1 = 2*np.power(lambda1,2) + np.power(lambda1,-4)
    #the index goes from
    return 2*(lambda1-lambda1**(-5))*\
            sum(
            param[k]*(k+1)*(I1 - 3)**(k+1-1)
            for k in range(n)
            )



def rp_PK1_ps(loadcase, model):
    """
    pure shear response function

    Parameters
    ----------
    loadcase : loadcase object
                loadcase x data are stretches (numpy array) and only
                one set of data is recognized and used
    model : model object

    Returns
    -------
    First component of the Piola-Kirchhoff stress tensor: numpy array

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail
    """
    model.fail = 0
    n = model.n_par
    lambda1 = loadcase.x[0][0]
    param = [model.parameters['c_'+str(i+1)] for i in range(n)]
    I1 = np.power(lambda1,2) + np.power(lambda1,-2) + 1

    return 2*(lambda1-lambda1**(-3))*\
            sum(
            param[k]*(k+1)*(I1 - 3)**(k+1-1)
            for k in range(n)
            )


#class
class rpol(Model):
    """
    Class that implements reduced polynomial hyper-elastic materials.

    Input data must be streches, outputs nominal stresses.
    See :ref:`model_attributes` for parameters common to all material
    models. Here only additional parameters will be listed.

    Parameters
    ----------

    as in parent class

    Returns
    -------

    as in parent class


    Attributes
    -----------------

    The same as the parent model class


    Other Parameters
    --------------------

    Material Parameters available are as listed in the constant
    `PARAMETER_NAMES`

    `c_i`: model moduli `i` in range [1..`n`]

    Methods
    -------
    In addition to the methods from Model class the following are available or
    redefined

    """

    PARAMETER_NAMES = ['c_'+str(i+1) for i in range(10)] + ['n']

    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC = {
                'uniaxial':rp_PK1_uniaxial,
                'biaxial': rp_PK1_biaxial,
                'pure shear': rp_PK1_ps,
                       }

    NAME = 'Ogden'

    def set_parameters(self, fixed_parameters = {}):
        """
        Builds the material parameters dictionary. Redefined from parent Model
        class as some modifications are needed to account for the varibale
        number of parameters


        Parameters
        ----------

        fixed_parameters: dictionary
            a dictionary containing keys correspondi to material
            parameters  and their fixed value

        Returns
        -------

        self.fixed_parameters, self.free_parameters, self.parameters:
            dictionaries
            dictionaries containing the free material parameters,
            the fixed ones and the union of the two

        """
        try:
            n = int(fixed_parameters['n'])
        except KeyError:
            fmlog.error("ERROR: Number of elements in Ogden model (n) not specified")
            fmlog.error("Quitting")
            sys.exit(333)


        self.PARAMETER_NAMES = ['c_'+str(i+1) for i in range(n)] + ['n']

        self.MAX_PARAM = len(self.PARAMETER_NAMES)

        #next fixed parameters must handled
        free_parameters = OrderedDict.fromkeys(self.PARAMETER_NAMES, None)

        for key in fixed_parameters:
            free_parameters.pop(key)
        #try to convert the parameters if this is the case
            try:
                fixed_parameters[key] = float(fixed_parameters[key])
            except ValueError:
                pass

        self.free_parameters = free_parameters
        self.fixed_parameters = fixed_parameters
        self.parameters = OrderedDict(  self.free_parameters.items() + \
                                        self.fixed_parameters.items() )

        n_free = len(self.free_parameters)

        if n_free <= 0:
            print "ERROR: no free parameters left for identification"
            sys.exit(666)


        self.n_par = n_free