# -*- coding: utf-8 -*-

"""
Author Information
Francesco Caimmi francesco.caimmi@polimi.it

Wrapper around OpenOpt solvers. This is an initial implementation, does not
support interalg

The reader is referred to `OpenOpt website
<http://http://openopt.org/Welcome>`_ for a detailed description of
the available solvers and their options.

OpenOpt is released under BSD License

"""

#Python imports
import logging

#Program modules imports
from fmat.FMLib import FMglobs
from fmat.FMLib.RSS import RSS


#Third Party imports
import openopt

#define the logger
fmlog = logging.getLogger(FMglobs.FMLOGGER)


#Func design import will be used in futer versions with func design support
#I know its ugly and stuff but that's the way it's meant to work
#from FuncDesigner import *

#a dictionary associating each solver with the respective OpenOpt package
SOLVER_DICT = {
                'interalg': 'GLP',
                'de': 'GLP',
                'ralg': 'NSP',
                'scipy_cobyla': 'NLP',
                'scipy_lbfgsb': 'NLP'
                }
# a list of solvers that cannot handle box problem
#for these solvers no box is set

NO_BOX_SOLVERS = ['scipy_leastsq', ]

def constrainer(model):
    """
    Function that builds the constraint array.
    All of the constraints are handled as non-linear constraints for there
    is no easy way to decide if a constraint is linear or non-linear

    Parameters
    -----------

    model:  material model object

    Returns
    -------

    h:  list of booleans
        vector of equality constraint
    c: list of booleans
        vector of inequality constraint
    """

    def h(x):
        """
        Builds the callable vector of equality constraints

        Parameters
        ----------

        x: array like
            material parameters

        Returns
        -------

        result: numpy array
            result array

        """
        eq_vector = []


        for key, value in model.constraints.iteritems():
            if value[1] == 'e':
                i = 0
                constraint = value[0]
                for key2 in model.free_parameters:
                    constraint = constraint.replace(key2,str(x[i]))
                    i += 1
                eq_vector.append(eval(constraint))

        if eq_vector:
            return eq_vector
        else:
            return None

    def c(x):
        """
        Builds the callable vector of inequality constraints

        Parameters
        ----------

        x: array like
            material parameters

        Returns
        -------

        result: numpy array
            result array

        """
        ineq_vector =[]



        for key, value in model.constraints.iteritems():
            i = 0
            if value[1] == 'i':
                constraint = value[0]
                for key2 in model.free_parameters:
                    constraint = constraint.replace(key2,str(x[i]))
                    i += 1
                ineq_vector.append(eval(constraint))

        if ineq_vector:
            return ineq_vector
        else:
            return None
    return h,c

def obj_wrapper(loadcase_dic, model):
    """
    Wrapper to build the objective function

    For some reason the args are not passed by pyOpt down to the objective
    function.
    Therefore the needed stuff loadcase_dic and model_original
    are provided via an higher order function

    Parameters
    ----------


    model: material model object

    loadcase_dic: dictionary
        dictionary of loadcases cotaining instances of cases class

    Returns
    -------

    f: float
        objective function value

    """
    def obj(x):
        """
        Objective function

        Parameters
        ----------

        x: array like
            placeholder for the parameter values

        Returns
        -------

        f:  float
            objective function value
        """
        i = 0

        for key in model.free_parameters:
            model.parameters[key] = x[i]
            i += 1

        if i != model.n_par:
            print "WARNING:free parameters at model level differ from \
            parameters supplied by the objective function"

        f = RSS(loadcase_dic, model)

        #verbose output
        fmlog.debug("Parameters")
        fmlog.debug(model.parameters)
        fmlog.debug("Residue: "+ str(f))

        return f

    return obj

def set_solver(solver_options, solver_name, solver ):
    """
    Modifies a OpenOpt problem instance and sets solver options.
    Supported options are those specific to every solver as supported by
    pyOpt. Refer to pyOpt documentation at
    `OpenOpt website <http://http://openopt.org/Welcome>`_
    for a list of available options

    Parameters
    ----------
    solver_options:
        dictionary with options (key=name, value=option value)

    solver_name: (string)
        solver name

    problem: OpenOpt problem instance
        problem instance

    Returns
    -------
    problem: OpenOpt problem instance
        modified `problem` instance

    """
    for key, value in solver_options.iteritems():
        if hasattr(solver,key):
            setattr(solver,key,value)
        else:
            print "WARNING: for solver ", solver_name, "there is no ",\
            key, "option.\nSkipping."

    return solver


def find_solution(solver_options, loadcase_dic, model, verbose,
                  op_mode = 'batch'):
    """
    Solves the optimization problem with a OpenOpt solver as specified by
    `solver_name` option in FMglobs

    Parameters
    ----------

    solver_options: dictionary
        contains options according to scheme {`option name`: `value`}

    loadcase_dic: dictionary
        dictionary of loadcases cotaining instances of cases class

    model: material model object

    verbose: boolean
        flag for verbose output

    Returns
    -------

    model: model object
        updated versione of the model object with parameter values set equal
        to the optmization values

    result: dictionary

        a dictionary containing the following {`key`:`pair`} data

        `evals`:
            number of function evaluations
        `min`:
            objective function value corresponding to the minimum
        `parameters`:
            dictionary of parameters following the same scheme as
            the one used for the model objects
        `time`:
            execution time

        Restructuring the results is necessary to  have a single,
        solver-independent postprocessing routine
    """



    solver_name = FMglobs.FMsolver_name.lower()

    #redefine the solver name by using the entry corresponding to solver_name
    #in the SOLVER_DICTIONARY variable
    FMproblem = openopt.__getattribute__(SOLVER_DICT[solver_name])()
    FMproblem.name = model.NAME

    #create a solver instance and set the options passed by solver_options
    #dictionary
    FMsolver = openopt.oosolver(solver_name)
    FMsolver = set_solver(solver_options, solver_name, FMsolver)

    if solver_name == 'interalg':
        import FMinteralg
        solution = FMinteralg.main(loadcase_dic, model, FMproblem, FMsolver)
    #
    #
        ###################################################################

    else:

        #define starting values and bounds
        FMproblem.x0 = model.start
        if solver_name not in NO_BOX_SOLVERS:
            FMproblem.lb = model.bounds[:,0]
            FMproblem.ub = model.bounds[:,1]

        #define constraints
        #also check that the defined initial values are actually feasible
        if model.constraints:
            equal, inequal = constrainer(model)
            if any(['e' in x for x in model.constraints.values()]):
                FMproblem.h = equal
            if any(['i' in x for x in model.constraints.values()]) :
                FMproblem.c = inequal
#               an attempt at flagging an infeasible start point
#               not working atm
#                if False in list(inequal(model.start)<0):
#                    print "WARNING: the specified initial values are not feasible"
#                    print "please check the constraints or the initial values"

        FMproblem.f = obj_wrapper(loadcase_dic, model)
        model.OBJ = FMproblem.f
    #   problem solution
        solution = FMproblem.solve(FMsolver)
#refactor output and output to file
    i = 0
    outfile = open(FMglobs.FMsolver_name+'.out','w')
    for key in model.free_parameters:
        model.parameters[key] = solution.xf[i]
        model.free_parameters[key] = solution.xf[i]
        outfile.write(key+'\t'+ str(solution.xf[i])+'\n')
        i += 1

    outfile.close()

    result = {'evals': solution.evals['f'],
              'min': solution.ff,
              'parameters': model.parameters,
              'time': solution.elapsed['solver_time']
              }

    return model, result
