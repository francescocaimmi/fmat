# -*- coding: utf-8 -*-
#author: Francesco Caimmi francesco dot caimmi @ polimi dot it
#see LICENSE.txt in the root source directory for licensing information
"""
This package provides Model class, a class used by the sub packages to define
their own material models. A note on conventions: the documentation of the
packages follows Numpy Conventions. Therefore the "parameteres" listed in the
class description are not to be understood as material parameters. These are
listed under the "Additional Parameters" heading.

Model Definition
====================

In the input file the model is defined by the following heading

**Input file usage**: ``[Model]``

To define a model one needs to specify the following keywords

name: the name of the material to use

**Input file usage**: ``name = material_model_name``

of course the material name must be among those available.

Fixed Parameters
----------------
It is possible to fix the values for some of the model parameters.
To do this use the following syntax in the input file:

**Input file usage**: ``parameter_name = value``
                    where `parameter_name` is the name of a parameter available
                    in the selected model. Refer to model documentation for a
                    list of parameters names.
                    Repeat as amany times as there are parameters to fix.


Additional keywords
-------------------

For every model the following options may be given:

1.Bounds: bounds on the parameters in the search space
        **Input file usage**: ``bounds = [[x_1_min,x_1_max]...[x_n_min,x_n_max]]``
            and an entry shall be given for every free parameters
            Defaults to a random numbers in the real interval [-100,100]

2.Initial values: intial values of the free parameters
        **Input file usage**: ``init = [x_1,...,x_n]``
            Defaults to a random numbers in the real interval [-100,100]




Constraints
-----------
Both equality and inequality contraints can be appplied to the model
parameters.
To specify them, list them separatedly using the constraint`n` keyword
where `n` is an increasing number.
The keyword accepts a python list of two elements, the constraint expression
and the constraint type, `i.e.` equality or inequality.

**Input file usage**: ``constraint`n` = [expression,i|e]``
                        replace `n` with numbers

`expression` is a symbolic expression containing the parameter names as
specified by the selected model (refer to its documentation).
`expression`, say g(**x**) where **x** is the parameter vector, is
interpreted as g(**x**)<0 if option `i` is specified, or as g(**x**)=0 if `e`
is specified.


Class Information
====================

All materials class instance provide the following attributes (constants, list
of strings)

PARAMETER_NAMES:
    a list of parameter names. In the input file the
    starting parameters shall be specified according to the
    list, skipping parameters for which fixed values are given.

MAX_PARAM:
    equals len(PARAMETER_NAMES), the maximum number of parameters

RES_FUNC:
    a dictionary providing the link between the admissible load cases
    for the material and the response function to calcluate ther
    response from the data for such a load case. For instance for
    Hooke's linear elastic material it something like
    {'uniaxial':hooke_uniaxial, 'poisson': hooke_poisson}.
    It is also used to check that the load cases types are actually
    available for the specified material model

NAME:
    the material name

"""

__all__ = ["ABAQUS","elastic","viscoelastic"]