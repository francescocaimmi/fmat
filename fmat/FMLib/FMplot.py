# -*- coding: utf-8 -*-
#see LICENSE.txt in the root source directory for licensing information

"""
Provides functions for plotting the data and the fit results

Routine Listing
-----------------
"""

def fm_xy_compare(plt, xdata, expy, modely, xlabel, ylabel):
    """
    For loadcases with a single control variable, plots model results for a
    a given set of parameters and the corresponding experimental data.

    Parameters
    ------------

    plt:    module
        pylab module from matplotlib

    xdata:  numpy array
        X-data to plot for a specific loadcase and for a specific value of
        some ordering parameters. Dimensions nvar*nmeasruements

    expy:   numpy array
        Y-data to plot. Dimension shape(`nvars`,`ndata`)

    modely:   numpy array
        model results to plot. Dimension shape(`nvars`,`ndata`)

    xlabel: string
        label for x-axis

    ylabel: string
        label for y-axis


    Returns
    --------

    plt:    module
        updates pylab module from matplotlib
    """

    #set labels
    plt.xlabel(xlabel, fontsize = 'x-large')
    plt.ylabel(ylabel, fontsize = 'x-large')
    #plot
    plt.scatter(xdata, expy,
                facecolors='none', edgecolors='black',
                s=40)
    plt.plot(xdata, modely, color = 'red', linestyle = '-')

    return plt

def fm_contour_compare(plt, xdata, expy, modely, xlabel, ylabel):
    """
    For loadcases with two control variables, plots model results for a
    a given set of parameters and the corresponding experimental data.

    Parameters
    ------------

    plt:    module
        pylab module from matplotlib

    xdata:  numpy array
        X-data to plot for a specific loadcase and for a specific value of
        some ordering parameters. Dimensions nvar*nmeasruements

    expy:   numpy array
        Y-data to plot. Dimension shape(`nvars`,`ndata`)

    modely:   numpy array
        model results to plot. Dimension shape(`nvars`,`ndata`)

    xlabel: string or list of strings
        label for x-axis

    ylabel: string or list of strings
        labels for y-axis


    Returns
    --------

    plt:    module
        updates pylab module from matplotlib
    """

    from scipy.interpolate import griddata
    import numpy as np

    x1min = np.min(xdata[0])
    x1max = np.max(xdata[0])
    x2min = np.min(xdata[1])
    x2max = np.max(xdata[1])
    x1i = np.linspace(x1min, x1max, 300)
    x2i = np.linspace(x2min, x2max, 300)

    #plot the experimental data
    yi = griddata((xdata[0], xdata[1]),
                  expy,
                 (x1i[None,:], x2i[:,None]), method='cubic')

    plt.subplot(121, aspect='equal')

    plt.contour(x1i, x2i, yi, 15, linewidths=0.5, colors='k')
    plt.contourf(x1i, x2i, yi, 15, cmap=plt.cm.jet)
    plt.scatter(xdata[0], xdata[1], marker='o', c='b', s=5)
    #plt.colorbar()

    #plot the model data

    zi = griddata((xdata[0], xdata[1]),
                  modely,
                 (x1i[None,:], x2i[:,None]), method='cubic')

    plt.subplot(122, aspect='equal')
    plt.contour(x1i, x2i, zi, 15, linewidths=0.5, colors='k')
    plt.contourf(x1i, x2i, zi, 15, cmap=plt.cm.jet)
    plt.scatter(xdata[0], xdata[1], marker='o', c='b', s=5)

    return plt
