fmat package
============

.. automodule:: fmat
    :members:
    :undoc-members:
    :show-inheritance:

Subpackages
-----------

.. toctree::

    fmat.FMLib
    fmat.FMcurses
    fmat.models
    fmat.solvers

