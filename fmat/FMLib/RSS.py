# -*- coding: utf-8 -*-
#see LICENSE.txt in the root source directory for licensing information

"""
Provides the function returning the Residual Sum of Squares (RSS) of the
problem for a given set of parameters stored in a model object
"""

#Python imports
import copy


#Third party imports
import numdifftools as nd
import numpy as np
from numpy import dot

def RSS(case_dictionary, model):
    """
    Calculates the RSS as

    .. math::
        RSS = \sum_{i=1}^{N} W_{ii}r_i^2

    where :math:`\mathbf{W}` is the weight matrix, :math:`\mathbf{r}` is the
    residue vector and :math:`N` the total number of data point from all the
    cases.
    This also sets the model attribute RSS

    Parameters
    -----------

    model:  model object
        the model object for which the RSS is to be calculated

    case_dictionary: dictionary
        a dictionary of load cases objects to be used to calcluate the RSS

    Returns
    --------

    RRS:    float
        the residual sum of squares
    """

    RSS = 0

    for loadcase in case_dictionary.itervalues():
            model.evaluate(loadcase)
            #using the model result lenght there is no risk to have loadcase.y
            #longer than the results

            RSS += (1.0/2.0)*np.sum(
                dot(loadcase.w,
                (model.result-loadcase.y[:len(model.result)])**2))

    model.RSS = RSS

    return RSS

def RSSgradient(cases_dict, model):
    """
    Calculates, at each data point, the gradient of the response functions
    with respect to the parameters.

    At the moment the implementation is based on numdifftools and is very very
    inefficient, as it requires a full model evaluation for each data point.
    Unsuitable at the moment for calculations other than the covariance matrix
    of simple models

    Parameters
    ----------
    cases_dict: dictionary
        dictionary of load cases

    model: model object

    Returns
    --------
    gradient: ndarray
        an array containing the gradient of the RSS sum with respect to the
        parameters. Parametes are ordered according to the model specification.
        The array has shape Nxn, where N is the total numer of data points
        available.
    """
    mcopy =  copy.deepcopy(model)
    n = model.n_par
    N = sum([len(case.y) for case in cases_dict.itervalues()])
    parameters = mcopy.free_parameters.values()


    gradient = np.zeros((N,n))
    current_point = 0

    for case in cases_dict.itervalues():

        for i in xrange(len(case.y)):

            def f(params):
                """
                Parameters
                ----------
                params: array like
                    the list of parameters
                """
                for j, key in enumerate(mcopy.free_parameters):#it works because parameter dict is ordered
                    mcopy.parameters[key] = params[j]
                mcopy.evaluate(case)
                return mcopy.result[i]

            grad = nd.Gradient(f)
            gradient[current_point] = grad(parameters)
            current_point += 1

    return gradient

def prova(cases_dict, model):
    """
    attempt at making the gradient calculation faster
    """
    n = model.n_par
    N = sum([len(case.y) for case in cases_dict.itervalues()])
    gradient = np.zeros((N,n))

    for case in cases_dict.itervalues():

        for i in xrange(len(case.y)):
            pass

    pass