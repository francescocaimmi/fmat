# -*- coding: utf-8 -*-
"""
An implementation of the hyperelastic eight chain Arruda-Boyce model for FMat.
The implementation follows ABAQUS derivation. In such an implementation, the
Langevin function appearing in the original model is replaced by a 5th order
Taylor expansion as a function of :math:`I_1`, the first invariant of the left
Cauchy-Green stretch tensor.

The actual formulation is for uncompressible materials.

Model Definition and Usage
==========================

Name
----

The name of the material specified in the input file is ``eightchain``

**Input file usage**: ``name = eightchain``

Material Parameters
-------------------


Material parameters belonging to the model are then

1. `lm`: locking stretch
2. `mu`: a parameter related to the shear modulus in the reference
         configuration



Response Functions
------------------

The response functions available are

1.uniaxial: key = uniaxial

2.pure shear: key = pure shear

3.Equibiaxial: key = biaxial

4.Generic Biaxial: key = genbiaxial



**Input file usage**:
                    under the heading ``[LoadCasen]`` specify
                    ``t_type = member of the list``

See the Module documentation for details on how to specify the input data
for the various load cases.

Module documentation
======================

"""

#Python imports
import numpy as np

try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

#FMat imports

from fmat.models.FMmodels import Model
from fmat.models.FMmodels import warn_for_data


#response functions

def ab_uniaxial(loadcase, model):
    """

    uniaxial response function

    Parameters
    ----------

    loadcase : loadcase object
        loadcase x data are stretches (numpy array) and only
        one set of data is recognized and used

    model : model object

    Returns
    -------

    stress: (numpy array)
        vector of uniaxial stresses

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail (unused at the moment)

    """
    model.fail = 0
    lambda1 = loadcase.x[0][0]
    lm = model.parameters['lm']
    mu = model.parameters['mu']
    C = model.CONSTANTS

    I1 = np.power(lambda1,2) + 2*np.divide(1,lambda1)
    modulus = 2*mu*sum([i*C[i-1]*np.power(I1,i-1)/(np.power(lm,2*i-2))
            for i in range(1,6)])
    stress = (lambda1-np.power(lambda1,-2))*modulus

    return stress

def ab_ps(loadcase, model):
    """

    pureshear response function

    Parameters
    ----------

    loadcase : loadcase object
        loadcase x data are pure shear stretches (numpy array) and only
        one set of data is recognized and used

    model : model object

    Returns
    -------

    stress: (numpy array)
        vector of pure shear stresses

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail (unused at the moment)

    """
    model.fail = 0
    lambda1 = loadcase.x[0][0]
    lm = model.parameters['lm']
    mu = model.parameters['mu']
    C = model.CONSTANTS

    I1 = np.power(lambda1,2) + np.power(lambda1,-2) + 1
    modulus = 2*mu*sum([i*C[i-1]*np.power(I1,i-1)/(np.power(lm,2*i-2))
            for i in range(1,6)])
    stress =(lambda1-np.power(lambda1,-3))*modulus

    return stress

def ab_equibia(loadcase, model):
    """

    biaxial response function

    Parameters
    ----------

    loadcase : loadcase object
        loadcase x data are biaxial stretches (numpy array) and only
        one set of data is recognized and used

    model : model object

    Returns
    -------

    stress: (numpy array)
        vector of pure shear stresses

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail (unused at the moment)

    """
    model.fail = 0
    lambda1 = loadcase.x[0][0]
    lm = model.parameters['lm']
    mu = model.parameters['mu']
    C = model.CONSTANTS

    I1 = 2*np.power(lambda1,2) + np.power(lambda1,-4)
    modulus = 2*mu*sum([i*C[i-1]*np.power(I1,i-1)/(np.power(lm,2*i-2))
            for i in range(1,6)])
    stress =(lambda1-np.power(lambda1,-5))*modulus

    return stress



###########
#class
###########
class eightchain(Model):
    """
    See :ref:`model_attributes` for parameters common to all material
    models. Here only additional parameters will be listed.

    Parameters
    ----------

    as in parent class

    Returns
    -------

    as in parent class


    Attributes
    -----------------

    The same as the parent model class


    Other Parameters
    --------------------

    Material Parameters available are as listed in the constant
    `PARAMETER_NAMES`
    'biaxial'
    `lm`: locking stretch
    `mu`: modulus

    In addition it provides the coefficient of the 5th order Taylor expansion
    of the Langevin function as CONSTANTS, a model class attribute

    Methods
    -------

    None in addition to parent class (see :ref:`model_attributes`)
    """

    PARAMETER_NAMES = ['lm','mu']

    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC = {'uniaxial':ab_uniaxial,
                'pure shear': ab_ps,
                'biaxial': ab_equibia}

    NAME = 'eightchain'

    #constants resulting from the Taylor expansion of the Langevin function
    CONSTANTS = np.array([1.0/2.0, 1.0/20.0, 11.0/1050.0, 19.0/7000.0,
                          519.0/673750.0])
