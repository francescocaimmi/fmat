# -*- coding: utf-8 -*-
#see LICENSE.txt in the root source directory for licensing information
"""

Implementation of the linear viscoelastic material with kernel described
by a Prony series for FMat.
The model is implemented as follows: for the relaxation modulus it is written
as

.. math::

    G(t) = G_0 \\left[ 1- \\sum^{n}_{i=1} g_i \\left(1-\\exp(t/\\tau_i) \\right)
    \\right]

where :math:`G` is the relaxation modulus as a function of time, :math:`G_0` is
the istantaneous shear modulus, :math:`\\tau_i` are the relaxation times of
each viscoelastic element n the series and :math:`g_i` are the normalized (with
respect to :math:`G_0`) shear moduli of each element.
The normalized shaer moduli should belong to the interval [0,1] and the
relaxation times should be greater than zero.

When used in the frequency domain the storage, :math:`G_s`, and the loss,
:math:`G_l`, moduli are defined by

.. math::

    G_s(\\omega) = G_0\\left( 1- \sum^{n}_{i=1} g_i \\right) + G_0
    \\sum^{n}_{i=1}\\frac{g_i \\tau_i^2 \\omega^2}{1 + \\tau_i^2 \\omega^2}

    G_l(\\omega) = \\sum^{n}_{i=1} \\frac{g_i \\tau_i \\omega}{1 + \\tau_i^2
    \\omega^2}

where :math:`\\omega` is the angular velocity [rad/s].

Note that the definitions follows ABAQUS FE package conventions.

Model Definition and Usage
==========================

Name
----

The name of the material specified in the input file is ``Prony``

**Input file usage**: ``name = Prony``

Material Parameters
-------------------

The number `n` of elements in the model (term in the Prony series) **must** be
specified


**Input file usage**: ``n = number``

Material parameters belonging to the model are then

1.`G_0`: Istantaneous shear modulus.

2.`g_i`:   Moduli of the Prony series, with `i` raging from 1 to `n`.

3. `tau_i`: Relaxation times corresponding to `g_i`, with `i` ranging from 1 to
`n`

By defaults, a constraint is imposed on the `g_i` so that they belong to [0,1]
and the relaxation times are assumed to be greater than 0. These defaults
can be overridden by specyfing the `bounds` keyword in the model specification.
`G_0` is taken by default to belong to the range 1-1e10.

Response Functions
------------------
The response functions available are

1.  relaxation modulus:  key = relaxation_modulus

2.  storage_modulus: key = storage_modulus

3.  loss modulus: key = loss_modulus



**Input file usage**:
                    under the heading ``[LoadCasen]`` specify
                    ``t_type = member of the list``

See the Module documentation for details on how to specify the input data
for the various load cases.

Module documentation
======================
"""

#python imports
import sys

try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

#numpy imports
import numpy as np

#FMAt imports

from fmat.models.FMmodels import Model

def relaxation_modulus(loadcase, model):
    """
    Routine Listing
    =================

    Yields the relaxation modulus

    Parameters
    ----------
    loadcase : loadcase object
                loadcase x data are times (numpy array) and only
                one set of data is recognized and used

    model : model object

    Returns
    -------

    modulus: numpy array
        the relaxation modulus


    """

    time = loadcase.x[0][0]
    #n is the number of terms in the sum, 0 exluded
    n = int(model.fixed_parameters['n'])

    G0 = model.parameters['G_0']
    params = [model.parameters['g_'+str(i+1)] for i in range(n)]+\
            [model.parameters['tau_'+str(i+1)] for i in range(n)]

    return G0*(
        1-sum(params[k]*(1-np.exp(-time/params[n+k])) for k in range(n))
        )

def storage_modulus(loadcase, model):
    """
    Returns the conservative modulus as a function of angular velocity for a
    linear viscoelastic material.

    Parameters
    ----------
    loadcase :  loadcase object
                loadcase x data are frequencies [Hz], that are converted
                internally to angular velocities [rad/s] (numpy array)
                and only one set of data is recognized and used

    model : model object

    Returns
    --------
    modulus: numpy array

    """
    f = 2*np.pi*loadcase.x[0][0]
    n = int(model.fixed_parameters['n'])
    G0 = model.parameters['G_0']
    params = [model.parameters['g_'+str(i+1)] for i in range(n)]+\
            [model.parameters['tau_'+str(i+1)] for i in range(n)]

    A = G0*(1-sum(params[i] for i in range(n)))

    B = G0*sum(
        (params[i] * params[i+n]**2 * f**2)/(1 + params[i+n]**2 * f**2)
        for i in range(n)
        )

    return A+B

def loss_modulus(loadcase, model):
    """
    Returns the loss modulus as a function of angular velocities for a linear
    viscoelastic material.

    Parameters
    ----------
    loadcase :  loadcase object
                loadcase x data are frequencies [Hz], that are converted
                internally to angular velocities [rad/s] (numpy array)
                and only one set of data is recognized and used

    model : model object

    Returns
    --------
    modulus: numpy array

    """
    f = 2*np.pi*loadcase.x[0][0]
    n = int(model.fixed_parameters['n'])
    G0 = model.parameters['G_0']
    params = [model.parameters['g_'+str(i+1)] for i in range(n)]+\
            [model.parameters['tau_'+str(i+1)] for i in range(n)]


    mod = G0*sum(
        (params[i] * params[i+n] * f)/(1 + params[i+n]**2 * f**2)
        for i in range(n)
        )

    return mod

class Prony(Model):
    """
    See :ref:`model_attributes` for parameters common to all material
    models. Here only additional parameters will be listed.

    Parameters
    ----------

    as in parent class

    Returns
    -------

    as in parent class


    Attributes
    -----------------

    The same as the parent model class


    Other Parameters
    --------------------

    Material Parameters available are as listed in the constant
    `PARAMETER_NAMES`

    `G_0`: istantaneous shear modulus
    `g_i`: Prony elements modului `i` in range [1..`n`]
    `tau_i`:  Prony relaxation times `i` in range [1..`n`]

    Methods
    -------
    In addition to the methods from Model class the following are available or
    redefined
    """

    PARAMETER_NAMES = ['G_0']+['g_'+str(i+1) for i in range(10)]+\
                        ['tau_'+str(i+1) for i in range(10)]+['n']

    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC = { 'relaxation_modulus': relaxation_modulus,
                'storage_modulus': storage_modulus,
                'loss_modulus': loss_modulus
                }

    NAME = 'Prony'

    fail = 0

    def set_parameters(self, fixed_parameters = {}):
        """
        Builds the material parameters dictionary. Redefined from parent Model
        class as some modifications are needed to account fo the varibale
        number of parameters


        Parameters
        ----------

        fixed_parameters: dictionary
            a dictionary containing keys correspondi to material
            parameters  and their fixed value

        Returns
        -------

        self.fixed_parameters, self.free_parameters, self.parameters:
            dictionaries
            dictionaries containing the free material parameters,
            the fixed ones and the union of the two

        """

        try:
            n = int(fixed_parameters['n'])
        except KeyError:
            print "ERROR: Number of elements in Ogden model (n) not specified"
            print "Quitting"
            sys.exit(333)

        self.PARAMETER_NAMES = ['G_0']+['g_'+str(i+1) for i in range(n)]+\
                        ['tau_'+str(i+1) for i in range(n)]+['n']

        self.MAX_PARAM = len(self.PARAMETER_NAMES)

        #next fixed parameters must handled
        free_parameters = OrderedDict.fromkeys(self.PARAMETER_NAMES, None)

        for key in fixed_parameters:
            free_parameters.pop(key)
        #try to convert the parameters if this is the case
            try:
                fixed_parameters[key] = float(fixed_parameters[key])
            except ValueError:
                pass

        self.free_parameters = free_parameters
        self.fixed_parameters = fixed_parameters
        self.parameters = OrderedDict(  self.free_parameters.items() + \
                                        self.fixed_parameters.items() )

        n_free = len(self.free_parameters)

        if n_free <= 0:
            print "ERROR: no free parameters left for identification"
            sys.exit(666)


        self.n_par = n_free