# -*- coding: utf-8 -*-
"""
Solvers module contains and defines the wrapper around the actual
solvers.

At the moment the folowing solver packages are supported:
    1. `pyOpt <http://www.pyopt.org/index.html>`_
    2. `OpenOpt <http://http://openopt.org/Welcome>`_


For options regarding the solvers the reader is referred to their documentation
As not every solver provided by those packages is supported in FMat,for a list
of solvers supported by FMat please refer to :ref:`fmglobs_doc`.


solvers usage
=============

In batch mode, solvers have a specific section in the input file. To define
a solver one needs just to select its name

**Input file usage**:
        ``[Solver]``
        ``name = solver_name``

Specific solver options can be specified by adding the option as an additional
keyword

**Input file usage**:
        ``option_name = option_value``

        For example, to set the maximum number of generations allowed in
        the NSGA2 solver to 1e5, the line would be
        ``maxGen = 1e5``

These will be passed to the solver package and added to the solver options.
A warning is issued if the option cannot be found.

Please refer to the above mentioned specific solver package documentation
for the options available for each solver.

developer information
=====================

Each subpackage provides a single function understandable by callers,
`i.e.` by FMat processing routines defined in FMLib which depends on the
operation mode.

The function must be named find_solution and posses the following call
signature:


`find_solution(solver_options, loadcase_dic, model, solver_name)`

where the symbols have the following meaning

**solver_options**:
    dictionary containing options for the solver
    according to scheme {'option name': value}

**loadcase_dic**:
    dictionary of loadcases cotaining instances of cases class

**model**:
    material model object defined according to materials available in
    models

**solver_name**
    is the solver name (string)

Such a function must do everthing which is needed to find the problem solution

Output must be a tuple composed of

**model**: model object
    updated versione of the model object with parameter values set equal
    to the optmization values

**result**: dictionary

        a dictionary containing the following {`key`:`pair`} data

        `evals`:
            number of function evaluations
        `min`:
            objective function value corresponding to the minimum
        `parameters`:
            dictionary of parameters following the same scheme as
            the one used for the model objects
        `time`:
            execution time

        Restructuring the results is necessary to  have a single,
        solver-independent postprocessing routine
"""
import FMpyOpt
import FMOpenOpt
__all__=["FMpyOpt","FMOpenOpt"]
