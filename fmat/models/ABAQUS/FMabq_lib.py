# -*- coding: utf-8 -*-


"""

Library containing utilities functions for ABAQUS integrated material models
mainly aimed at job and post-processing execution and control.

Routine Listing
---------------
"""

#Python imports
import sys
import subprocess
import os
import time
import numpy as np
import shutil
from scipy.interpolate import griddata
import ast
#import pdb

#FMat imports
from fmat.FMLib import FMglobs

def job_monitor(job_name):
    """
    Monitors a job log file to see if execution is completed or errors were
    raised

    Parameters
    -----------

    job_name:   string
        a sting identyfing the job name

    Returns
    ---------

    errorflag:  boolean
        boolean indicating if errors took palce during job execution. False if
        no error was raised.
    """
    time.sleep(10)
    running = True
    wait_time = 0
    errorflag = False
    while running:
        time.sleep(10)

        try:
            log_file = open(job_name+'.log', 'r')
            log_content = log_file.readlines()
            #closing the file immediatly is better to prevent
            #stale NFS handle errors if files are stored on a NFS filesytem
            log_file.close()
        except IOError:
            time.sleep(60)
            wait_time += 10
            if wait_time >> 3600 :
                print "Warning"
                print "FMat waited for more than one hour for the log file"
                print "of job ",job_name," to appear."
                print "Please check if everything is going smooth"
        else:
            for line in log_content:

                if 'abaqus error' in line.lower():

                    errorflag = True
                    return errorflag

                elif 'completed' in line.lower():

                    running = False

    return errorflag


def run_analysis(job_name = 'Job-1', cmd_options = ''):
    """
    Runs an ABAQUS analysis

    Parameters
    ----------
    job_name:   string or dictionary
        job to be execute

    cmd_options:
        string of additional options for ABAQUS solver

    Returns
    -------
    errorflag: boolean
    """
    errorflag = False

    #check if the abaqus executable is ok

    if FMglobs.abaqus_cmd == None:
        print "No Abaqus executable is specified in the configuration file"
        #the reinstall thing will not become true before version 0.4 at least
        print "Specify it or Reinstall FMat"
        sys.exit(333)


    #build the args vector
    abaqus_args = [FMglobs.abaqus_cmd, '-j', job_name]
    if cmd_options != None:
        abaqus_args += cmd_options.split()

    #start the analysis

    abaqus_process = subprocess.Popen(abaqus_args, stdout=subprocess.PIPE)
    print "Executing ABAQUS job", job_name
    output, error = abaqus_process.communicate()

    if error != None or abaqus_process.returncode != 0:
        errorflag = True
        return errorflag
    #abaqus process yield immediatly to the preprocessor. To see what is going
    #on the log file shall be checked

    errorflag = job_monitor(job_name)

    return errorflag

def run_postprocessing(loadcase, model):
    """
    Runs an ABAQUS postprocessing script to obtain data from the odb

    Uses linear interpolation with extrapolation to zero
    as provided by SciPy griddata function, so the user must be sure to provide
    a fair number of data points for the interpolations to be sensible

    .. _run_post:

    Parameters
    ----------

    loadcase:    loadcase object

    model:   a model instance

    Returns
    -------

    errorflag: boolean
        a flag for errors in execution

    result: numpy array
        array containing the model response for the given data
    """

    result = None
    errorflag = False

    path=os.getcwd()+'//'

    if FMglobs.abaqus_cmd == None:
        print "No Abaqus executable is specified in the configuration file"
        print "Specify it or Reinstall FMat"
        sys.exit(333)

    # two cases from here on
    #first case: a single post-processing case for every loadcase
    if type(model.post_script) == str:
        post_cmd = model.post_script.split()
        odb_name = model.input_file.split('.inp')[0]+'_fmat.odb'
    #second case: a dictionary of post processing scripts
    elif type(model.post_script) == dict:
        #if we have apost processing script do the standard things
        if model.post_script[loadcase.name]:

            post_cmd = [model.post_script[loadcase.name]]
            odb_name = model.input_file[loadcase.name].split('.inp')[0]\
                    +'_fmat.odb'

        else:
        #otherwise no post processing is made
            post_cmd = None
    else:
        post_cmd = None


    if model.post_output == None:

        #the post-processing script does it all
        script = __import__(post_cmd)
        errorflag, result = script.main(loadcase.x, model)
        return errorflag, result
    #the posprocessing script writes ouptut files or output files are magically
    #already there
    else:
        if post_cmd == None:
            if FMglobs.FM_no_post_script == False:
                print "WARNING: No specified post execution script for the"
                print "Loadcase", loadcase.name
                print "The program expects to find the result files as"
                print "specified in the model option post_output"
                FMglobs.FM_no_post_script = True
        else:
            #build the args vector
            abaqus_args = [FMglobs.abaqus_cmd, 'python'] +\
                        post_cmd+\
                        ['--fmat_input', \
                        path+odb_name]
            #run the script
            abaqus_process = subprocess.Popen(abaqus_args,
                                              stdout=subprocess.PIPE)
            print "Executing ABAQUS post ", post_cmd
            output, error = abaqus_process.communicate()
            if abaqus_process.returncode != 0:
                errorflag = True
                return errorflag, result

        #manage the post processing output converting to a list

        if isinstance(model.post_output, str):
            result_obj = [model.post_output]
        elif isinstance(model.post_output, dict):
            result_obj = model.post_output[loadcase.name]
        else:
            result_obj = model.post_output

        #check if there is correspondence between the loadcase data and the
        #output data
        if len(loadcase.x) != len (result_obj):
            print "WARNING: the number of model output files is different from"
            print "the number of load case data files"
            print "please check the data definitions"

        result = np.array([])

        for index, filename in enumerate(result_obj):

            if FMglobs.FMverbose:
                print "Post processing",  os.path.abspath(filename)

            try:
                data = np.genfromtxt(filename, unpack = True)
            except IOError:
                print "WARNING Cannot read ",filename
                print "during ABAQUS post-processing"
                errorflag = True
                return errorflag, result

            res_y = data[:][-1]
            res_x = data[:][:-1].transpose()
            #results are constructed as linear interpolation of the results
            #given: make sure you have enough points and cover the whole
            #data field
            result = np.append(result,
                               griddata(res_x, res_y,
                               np.column_stack([i for i in loadcase.x[index]]),
                               fill_value = 0, method = 'linear')
                              )

        return errorflag, result

def input_file_names(loadcase, model):

    """
    Defines the new and old input filenames

    Parameters
    ----------

    loadcase: (loadcase object)
        loadcase data are generic data consistent with what happens
        in the user supplied postprocessing script

    model: (model object)

    Returns
    -------

    filename: (string)
        file name to be opened

    orname: (string)
        old filen name to be modified


    """
    orname = None
    #set the new file
    #two cases must be handled: dictionary for input files or simple string
    #meaning a single input file for all load cases
    if type(model.input_file) == str:
        orname = model.input_file

    elif type(model.input_file) == dict:
        orname = model.input_file[loadcase.name]
    #must handle the case of no input file, for which no job is run and post
    #processing does it all
    if orname:
        #we have a filename
        filename = orname.split('.inp')[0]+'_fmat.inp'

        try:
            shutil.copyfile(orname, filename)
        except IOError:
            print "ABAQUS input filename does not exist or wrong diretory"
            sys.exit(333)

    else:
        filename = None

    return filename, orname


def manage_optional_dictionaries(key,value,loadcase):
    """
    Processes some parameters in input files and returns the loadcase object
    with attributes set


    Parameters
    -----------

    key: string
        key to process (input_file,post_script etc..)

    value: string
        value read from FMat input to be processed

    loadcase:   loadcase object
        loadcase object whose attributue `key` is to be set

    Returns
    -------

    None:   modifies loadcase

    """
    try:#it is a dictionary
        tmp = ast.literal_eval(value)
        setattr(loadcase,key,tmp )
        #now we must convert the values in the dictionary
        #to lists
        tmp2 = getattr(loadcase,key)
        for subkey in tmp2:
            new_value = tmp2[subkey].split(',')
            if len(new_value) == 1:
                #stringa ad un elemento solo: non c'è bisogno di
                #tenerlo come tale
                tmp2[subkey] = new_value[0].strip()
            else:
                for index in xrange(len(new_value)):
                    new_value[index] = new_value[index].strip()
                tmp2[subkey] = new_value
        del tmp
    except (ValueError,SyntaxError):
        #in this case it is a string
        new_value = value.split(',')
        if len(new_value) == 1:
            setattr(loadcase, key, new_value[0])
        else:
            setattr(loadcase, key, new_value)