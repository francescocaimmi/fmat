# -*- coding: utf-8 -*-
"""
.. py:module:: FMogden:

Implementation of the Ogden elastic material for FMat

Model Definition and Usage
==========================

Name
----

The name of the material specified in the input file is ``Ogden``

**Input file usage**: ``name = Ogden``

Material Parameters
-------------------

To use Ogden material the number `n` of elements in the model (term in the
summation) **must** be specified

**Input file usage**: ``n = number``

Material parameters belonging to the model are then

1. `alpha_i`: Exponents of the model terms

2. `mu_i`: Poisson's coefficient


there are as many couples (`alpha_i`, `mu_i`) as n
(`i.e.` `i` in range [1..`n`])

Response Functions
------------------
The response functions available are

1.uniaxial: key = uniaxial

2.pure shear: key = pure shear

3.Equibiaxial: key = biaxial

4.Generic Biaxial: key = genbiaxial


**Input file usage**:
                    under the heading ``[LoadCasen]`` specify
                    ``t_type = key``

Note that genbiaxial returns the in-plane principal stresses corresponding to
a generic biaxial loading. It assumes that the direction of the stress to be
returned corresponds to the one of the first stretch that must is supplied.

See the Module documentation for details on how to specify the input data
for the various load cases.

Module documentation
======================

"""

#Python imports
import logging
import sys
import numpy as np

#FMat imports

from fmat.models.FMmodels import Model
from fmat.models.FMmodels import warn_for_data
from fmat.FMLib import FMglobs

try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

#prepare the logger
fmlog = logging.getLogger(FMglobs.FMLOGGER)


#response functions

def ogden_PK1_uniaxial(loadcase, model):
    """
    Routine Listing
    ----------------

    uniaxial response function

    Parameters
    ----------
    loadcase : loadcase object
        loadcase x data are stretches (numpy array) and only
        one set of data is recognized and used

    model : model object

    Returns
    -------
    First component of the Piola-Kirchhoff stress tensor: numpy array

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail
    """
    model.fail = 0
    n = model.n_par/2
    lambda1 = loadcase.x[0][0]
    param = [model.parameters['alpha_'+str(i+1)] for i in range(n)]+\
            [model.parameters['mu_'+str(i+1)] for i in range(n)]
    return sum(
            param[k+n]*(lambda1**(param[k]-1)-lambda1**(-0.5*param[k]-1))
            for k in range(n)
            )


def ogden_PK1_biaxial(loadcase, model):
    """
    equi-biaxial response function

    Parameters
    ----------

    loadcase : loadcase object
                loadcase x data are stretches (numpy array) and only
                one set of data is recognized and used
    model : model object

    Returns
    -------

    First component of the Piola-Kirchhoff stress tensor: numpy array

    fmodel.fail:    (int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail
    """
    model.fail = 0
    n = model.n_par/2
    lambda1 = loadcase.x[0][0]
    param = [model.parameters['alpha_'+str(i+1)] for i in range(n)]+\
            [model.parameters['mu_'+str(i+1)] for i in range(n)]

    return sum(
                param[k+n]*(lambda1**(param[k]-1)-lambda1**(-2*param[k]-1))
                for k in range(n)
                )


def ogden_PK1_ps(loadcase, model):
    """
    pure shear response function

    Parameters
    ----------
    loadcase : loadcase object
                loadcase x data are stretches (numpy array) and only
                one set of data is recognized and used
    model : model object

    Returns
    -------
    First component of the Piola-Kirchhoff stress tensor: numpy array

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail
    """
    model.fail = 0
    n = model.n_par/2
    lambda1 = loadcase.x[0][0]
    param = [model.parameters['alpha_'+str(i+1)] for i in range(n)]+\
            [model.parameters['mu_'+str(i+1)] for i in range(n)]

    return sum(
                param[k+n]*(lambda1**(param[k]-1)-lambda1**(-1*param[k]-1))
                for k in range(n)
                )

def ogden_pk1_genbiaxial(loadcase, model):
    """
    generic biaxial response function. The input data is a (ndatax2) shape
    array containing the streches in two principal directions. Consistently
    with FMAT "a single output variable for loadcase", it returns only a
    single principal stress, the one corresponding to the first component
    of stretch passed in. In order to use both the stress components of a
    generic biaxial test, two Loadcases must be specified, with the same
    strech data but with switched columns and different stress measures.

    Parameters
    ----------

    loadcase : loadcase object
                loadcase x data are stretches (numpy array) for the two
                principal directions
    model : model object

    Returns
    ---------

    First component of the Piola-Kirchhoff stress tensor: numpy array

    fmodel.fail:    (int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail

    """
    model.fail = 0
    n = model.n_par/2
    lambda1 = loadcase.x[0][0]
    lambda2 = loadcase.x[0][1]
    param = [model.parameters['alpha_'+str(i+1)] for i in range(n)]+\
            [model.parameters['mu_'+str(i+1)] for i in range(n)]

    return sum(
            param[k+n]*(lambda1**(param[k]-1)-
                np.multiply(lambda1**(-1-param[k]),lambda2**(-param[k])))
            for k in range(n)
            )
#class
class Ogden(Model):
    """
    Class that implements Ogden's hyper-elastic materials.

    Input data must be streches, outputs nominal stresses.
    See :ref:`model_attributes` for parameters common to all material
    models. Here only additional parameters will be listed.

    Parameters
    ----------

    as in parent class

    Returns
    -------

    as in parent class


    Attributes
    -----------------

    The same as the parent model class


    Other Parameters
    --------------------

    Material Parameters available are as listed in the constant
    `PARAMETER_NAMES`

    `alpha_i`: Ogden's model exponents `i` in range [1..`n`]
    `mu_i`: Ogden's model moduli `i` in range [1..`n`]

    Methods
    -------
    In addition to the methods from Model class the following are available or
    redefined

    """

    PARAMETER_NAMES = ['alpha_'+str(i+1) for i in range(10)]+\
                        ['mu_'+str(i+1) for i in range(10)]+['n']

    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC = {
                'uniaxial':ogden_PK1_uniaxial,
                'biaxial': ogden_PK1_biaxial,
                'pure shear': ogden_PK1_ps,
                'genbiaxial': ogden_pk1_genbiaxial,
                       }

    NAME = 'Ogden'

    def set_parameters(self, fixed_parameters = {}):
        """
        Builds the material parameters dictionary. Redefined from parent Model
        class as some modifications are needed to account for the varibale
        number of parameters


        Parameters
        ----------

        fixed_parameters: dictionary
            a dictionary containing keys correspondi to material
            parameters  and their fixed value

        Returns
        -------

        self.fixed_parameters, self.free_parameters, self.parameters:
            dictionaries
            dictionaries containing the free material parameters,
            the fixed ones and the union of the two

        """
        try:
            n = int(fixed_parameters['n'])
        except KeyError:
            fmlog.error("ERROR: Number of elements in Ogden model (n) not specified")
            fmlog.error("Quitting")
            sys.exit(333)


        self.PARAMETER_NAMES = ['alpha_'+str(i+1) for i in range(n)]+\
                        ['mu_'+str(i+1) for i in range(n)]+['n']

        self.MAX_PARAM = len(self.PARAMETER_NAMES)

        #next fixed parameters must handled
        free_parameters = OrderedDict.fromkeys(self.PARAMETER_NAMES, None)

        for key in fixed_parameters:
            free_parameters.pop(key)
        #try to convert the parameters if this is the case
            try:
                fixed_parameters[key] = float(fixed_parameters[key])
            except ValueError:
                pass

        self.free_parameters = free_parameters
        self.fixed_parameters = fixed_parameters
        self.parameters = OrderedDict(  self.free_parameters.items() + \
                                        self.fixed_parameters.items() )

        n_free = len(self.free_parameters)

        if n_free <= 0:
            print "ERROR: no free parameters left for identification"
            sys.exit(666)


        self.n_par = n_free