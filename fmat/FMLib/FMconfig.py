# -*- coding: UTF-8 -*-
#*******************************************************************************
#Author Information
#Francesco Caimmi, Ph.D.
#Laboratorio di Ingegneria dei Polimeri
#Politecnico di Milano - Dipartimento di Chimica, Materiali e Ingegneria
#Chimica
#"Giulio Natta"
#
#P.zza Leonardo da Vinci, 32
#I-20133 Milano
#Tel. +39.02.2399.4711
#Fax +39.02.7063.8173
#
#francesco.caimmi@polimi.it
#Skype: fmglcaimmi
#GPG Public Key : http://goo.gl/64dDo
#*******************************************************************************
#IMconfig.py is the modulus that reads the configuration options for I.Mat,
#sets defaults values for missing options and queries the system for additional
#infomation.
#Configuration options are stored in IMatConfig.cfg file in the main program
#directory.
#*******************************************************************************
#*******************************************************************************
#This file was first create on Mar 11, 2011 16:06
#This file was last edited on Sept 14, 2011 12:20 (fc)
#*******************************************************************************
#*****************PROGRAM BEGINS HERE*******************************************
def main(configfile):
#DOC STRING MAIN

    """
    FMconfig.py is the configuration reader utility for FMat. 
    
    Uses ConfigParser Python modulus and the configuration file is

    INPUT:
    None

    OUTPUT
    a tuple made up of the following variables
    --General variables
    os_type: operating system
    --Abaqus interface variables
    abaqus_dir:abaqus installation directory
    
    """
    
    import os
    import ConfigParser
    print os.getcwd()
    return 1
#call the modulus from command line
if __name__ == "__main__":
    main()
#*****************PROGRAM ENDS HERE*******************************************