# -*- coding: utf-8 -*-
"""
Class for ABAQUS(C) cohesive material models

Model Definition and Usage
==========================

The reader is referred to ABAQUS documentation for a detailed description
of the cohesive laws which can be prescribed.Not every option provided by
ABAQUS is supported. The current implementation only support power law mixed
mode ratios.

Name
----

The name of the material specified in the input file is ``ABAQUS_cohesive``

**Input file usage**: ``name = ABAQUS_cohesive``

Material Parameters
-------------------

Material parameters belonging to the model are

1.  `Kn, Kt1, Kt2`:
        elastic stiffnesses

2.  `d1,d2,d3`:
       damage initiation thresholds for the respective criterion, can
       be stresses or deformations, see ABAQUS manual

3.   `G1,G2,G3`:
        mode I, II, III fracture energies (for energy based damage
        evolution only)

4.  `delta_max`:
        maximum effective displacement at fracture (for displacement
        based damage evolution only)


Response Functions
------------------
The response functions available are

1.generic:  key = generic
        generic response function

**Input file usage**:
                    under the heading ``[LoadCasen]`` specify
                    ``t_type = key``

See the Module documentation for details on how to specify the input data
for the various load cases.

Module documentation
======================
"""
#Python imports
import sys
import numpy as np
try:
    from collections import OrderedDict
except ImportError:
    from ordereddict import OrderedDict

#Internal imports
from fmat.models.FMmodels import Model
from fmat.models.ABAQUS.FMabq_lib import run_analysis
from fmat.models.ABAQUS.FMabq_lib import run_postprocessing
from fmat.models.ABAQUS.FMabq_lib import input_file_names
from fmat.models.ABAQUS.FMabq_lib import manage_optional_dictionaries



def generic(loadcase,model):
    """
    generic response function


    Parameters
    ----------
    loadcase:  loadcase object
        loadcase data are generic data consistent with what appens
        in the user supplied postprocessing script

    model : model object

    Returns
    ---------

    generic: (numpy array)
        output as defined by model.post_script


    """


    error_flag = False
    model.fail = 0

    #define original and modified input file names
    filename, orname = input_file_names(loadcase, model)
    #check if we have an input file
    if filename != None:

        abq_input_file = open(filename, 'w')

        #copy the parameters to the input file

        with open(orname,'r') as old_input:
            for line in old_input:

                if '*Heading' in line:
                    abq_input_file.write(line)
                    abq_input_file.write('*PARAMETER\n')
                    for key in model.parameters:
                        #print key,model.parameters[key]
                        abq_input_file.write(key+' = '
                                            +str(model.parameters[key])+'\n')
                #find a place to put the elastic constants. The first case
                #corresponds to cohesive elements, the second one to cohesive
                #surfaces

                elif (("*Elastic" and "type=TRACTION") \
                    or "*Cohesive Behavior") in line:
                    abq_input_file.write(line)
                    abq_input_file.write('<Kn>,<Kt1>,<Kt2>\n')
                    #skip the next line in the old file
                    line = next(old_input)
                elif '*Damage Initiation' in line:
                    abq_input_file.write(line)
                    abq_input_file.write('<d1>,<d2>,<d3>\n')
                    #skip the next line in the old file
                    line = next(old_input)
                elif '*Damage Evolution' in line:
                    abq_input_file.write(line)
                    if model.evolution_type.lower() == 'displacement':
                        abq_input_file.write('<delta_max>\n')
                    else:
                        abq_input_file.write('<G1>,<G2>,<G3>\n')
                    #skip the next line in the old file
                    line = next(old_input)
                else:
                    abq_input_file.write(line)

        abq_input_file.close()

        #execute the job

        error_flag = run_analysis(job_name = filename.split('.inp')[0] ,
                                  cmd_options = model.abaqus_options)

        #run the post processing script

        error_flag, result = run_postprocessing(loadcase, model)

    #we do not have an input file; assume the post-processing script will
    #do all the job
    else:
        error_flag, result = run_postprocessing(loadcase, model)

    if error_flag == True: #something went wrong
        model.fail = 1
        return np.ones(len(loadcase.y))

    return result


class ABAQUS_cohesive(Model):
#TODO implement support for temperature dependent fields and other mixed mode
#laws

    """

    The reader is referred to ABAQUS manual for details on the parameters.
    Not every option provided by ABAQUS is supported. The current
    implementation only support power law mixed mode ratios


    Parameters
    ----------

    Returns
    -------

    Attributes
    ------------------


    evolution_type:
            damage evolution law ENERGY|DISPLACEMENT
    power:
        exponent for BK or power law damage evolution

    softening:
        softening type LINEAR|EXPONENTIAL



    In addition to Model attributes and ABAQUS model common options as
    described in the models.ABAQUS documentation, ABAQUS_cohesive instances
    also have the following members, which must be specified in the input file
    Temperature dependent field data are not yet supported

    Additional Parameters
    ---------------------


    Parameters to be passed to the subroutine are divided in actual material
    parameters (stored in the constant MATERIAL_PARAMETERS ) and model
    mandatory arguments (stored in MODEL_MANDATORY ) and model options
    (in MODEL_OPTIONS).
    These list are joined to provide the PARAMETER_NAMES constant

    `Kn, Kt1, Kt2`:
        elastic stiffnesses

    `d1,d2,d3`:
        damage initiation thresholds for the respective criterion, can
        be stresses or deformations, see ABAQUS manual

    `G1,G2,G3`:
        mode I, II, III fracture energies (for energy based damage
        evolution only)

    `delta_max`:
        maximum effective displacement at fracture (for displacement
        based damage evolution only)


    Methods
    -------

    In addition to parent class Model methods, the following methods are
    available or redefined

    """
    MATERIAL_PARAMETERS = ['Kn','Kt1','Kt2', 'd1','d2','d3',
                           'G1', 'G2', 'G3', 'delta_max']

    MODEL_MANDATORY = ['evolution_type', 'power', 'softening',
                       'post_script',]

    MODEL_OPTIONS =['input_file', 'abaqus_options', 'post_output',]

    PARAMETER_NAMES = MATERIAL_PARAMETERS + MODEL_MANDATORY + MODEL_OPTIONS


    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC={'generic':generic,}

    NAME = 'ABAQUS_cohesive'


    def set_parameters(self,fixed_parameters = {}):
        """
        Builds the material parameters dictionary. Redefined from parent Model
        class

        Parameters
        ----------

        fixed_parameters: dictionary
            a dictionary containing keys correspondi to material
            parameters  and their fixed value

        Returns
        -------

        self.fixed_parameters, self.free_parameters, self.parameters:
            dictionaries
            dictionaries containing the free material parameters,
            the fixed ones and the union of the two

        """
       #creates parameter dictionary
        parameters = OrderedDict.fromkeys(self.PARAMETER_NAMES, None)

        #set fixed parameters and option
        for key in fixed_parameters.keys():
            parameters[key] = fixed_parameters[key]


        #some parameters that must be specified are actually options and not
        #material parameters; me must therefore remove them and store the
        #information elsewhere this is done twice for mandatory and optional
        #model parameters
        for key in self.MODEL_MANDATORY:

            try:
                value = parameters.pop(key)
                if key == 'post_script':
                    manage_optional_dictionaries(key, value, self)
                else:
                    setattr(self, key, value)
                fixed_parameters.pop(key)

            except KeyError:
                print "ERROR: unspecified ", key, "for the material model"
                sys.exit(666)

        for key in self.MODEL_OPTIONS:

            try:
                value = parameters.pop(key)
                if key == 'input_file':
                    manage_optional_dictionaries(key, value, self)
                else:
                    setattr(self, key, value)
                fixed_parameters.pop(key)
            except KeyError:
                setattr(self, key, None)
        #next fixed parameters must handled
        free_parameters = OrderedDict.fromkeys(self.MATERIAL_PARAMETERS, None)

        #check for different alternatives
        if self.evolution_type.lower() == 'displacement':
            free_parameters.pop('G1')
            free_parameters.pop('G2')
            free_parameters.pop('G3')
        else:
            free_parameters.pop('delta_max')



        for key in fixed_parameters:
            free_parameters.pop(key)

        #try to convert the parameters if this is the case
            try:
                fixed_parameters[key] = float(fixed_parameters[key])
            except ValueError:
                pass

        self.free_parameters = free_parameters
        self.fixed_parameters = fixed_parameters
        self.parameters = OrderedDict(  self.free_parameters.items() + \
                                        self.fixed_parameters.items() )

        n_free = len(self.free_parameters)

        if n_free <= 0:
            print "ERROR: no free parameters left for identification"
            sys.exit(666)

        self.n_par = n_free