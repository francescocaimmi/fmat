.. fmat documentation master file, created by
   sphinx-quickstart on Wed Oct 17 14:48:20 2012.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to FMat's documentation!
================================
FMat is a software for the identification of the parameters appearing in 
material consititutive laws through inverse analysis. Its strenght lies in the 
ability to perform simultaneous fits on different datasets obeying to different 
response functions.

Contents:

.. toctree::
    :maxdepth: 3
        
    fmat.FMcurses
    fmat.FMLib
    fmat.models
    fmat.solvers


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

