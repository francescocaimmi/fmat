# -*- coding: utf-8 -*-
#see LICENSE.txt in the root source directory for licensing information
"""
The modified Bergstrom Boyce model for non linear viscoelastic behaviour of
elastomers.

The model is based on Bergstom, Boyce, 2001. Mechanics of Materials, 33,
523-530. However the non linear springs are implemented using Mooney-Rivlin
elements.

Model Definition and Usage
==========================

Name
----

The name of the material specified in the input file is ``BB``

**Input file usage**: ``name = BB``

Material Parameters
-------------------

Material parameters belonging to the model are

For the elastic equilibrium network

1. `a10`: first elstic constant of the mooney-rivlin element
2. `a01`: second elstic constant of the mooney-rivlin element

For the elements describing deviation from equilibrium

3. `b10`: first elstic constant of the mooney-rivlin element
4. `b01`: second elstic constant of the mooney-rivlin element

For the viscoplastic network, whose deformation rate intensity is given by

.. math::
    \\dot{\\gamma} = A (\\lambda^{cr}-1+E)^C \\tau^m

where :math:`\\lambda^{cr}` is the effective creep strain in the element,
:math:`\\tau` is the effective stress in the element and
the parameters are then

5. `m`: the exponent of the stress in the non linear flow rule
6. `C`: the exponent of the pre-factor. Should be in [-1,0]
7. `E`: the prefactor stabilization constant. Should be very small, like 1e-3,
        and shall not influence much te results except for small stresses
8. `A`: reference strain rate divided by a reference stress level


Response Functions
------------------

The response functions available are

1.uniaxial load-unload: key = loun


**Input file usage**:
                    under the heading ``[LoadCasen]`` specify
                    ``t_type = member of the list``

See the Module documentation for details on how to specify the input data
for the various load cases.

Module documentation
======================

"""


#Python imports
import numpy as np
from scipy.integrate import odeint


#FMat imports

from fmat.models.FMmodels import Model
from fmat.models.FMmodels import warn_for_data


#response functions

def BB_loadunload(loadcase, model):
    """
    Uniaxial loading unloading response function. Describes a load-unload
    cycle at constant stretch velocity. The loadcase must specify as
    options v, the stretch rate in s**-1, and t1, the time at maximum 
    stretch when the speed is reversed.

    Parameters
    ----------

    loadcase : loadcase object
        loadcase x data are time points, while y data are nominal stress
        values. Must have v and t1 attributes (see above).

    model : model object

    Returns
    -------

    stress: (numpy array)
        vector of pure shear stresses

    model.fail:(int)
        flag specifing if the evaluation was succesfull
        O for success, 1 for fail (unused at the moment)

    """
    model.fail = 0
    timepoints = loadcase.x[0][0]
    l0 = [1,1] #initial stretches in element A and C
    a10 = model.parameters['a10']
    a01 = model.parameters['a01']
    b10 = model.parameters['b10']
    b01 = model.parameters['b01']
    m = model.parameters['m']
    C = model.parameters['C']
    E = model.parameters['E']
    A = model.parameters['A']
    v = float(loadcase.v)
    t1 = float(loadcase.t1)
    #integrate the stretches
    args = tuple([b10,b01,m,C,E,A] + [v,t1])
    res = odeint(load_unload_uniaxial_integrator, l0, timepoints, args = args)
    la = res[:,0]
    lc = res[:,1]
    lb = la/lc
    #calculate the stresses
    #cauchy stresses
    T_a = sigma_b(la,[a10,a01])
    T_b = sigma_b(lb,[b10,b01])
    #nominal stresses
    P = (T_a + T_b)/la
    
    return P
    
def load_unload_uniaxial_integrator(y, t, *args):
    """
    For the uniaxial load-unload loadcase,the differential equation system to 
    be solved dy/dt= integrator(y,t), where y is the vector [la,lc], 
    stretches in the equilibrium network and in the dashpot.

    Parameters
    -----------
    y:array
        [la,lc], stretches in the equilibriumnetwork and in the
    t: time
    args:list of floats
        b10,b01,m,C,E,A,v and t1

    Returns
    ---------
    values: np.array
        the value of dy/dt at (y,t)

    """
    b10 = args[0]
    b01 = args[1]
    m = args[2]
    C = args[3]
    E = args[4]
    A = args[5]
    v = args[6]
    t1 = args[7]

    values = np.zeros(2)
    #stretch in the equilibrium network
    if t <= t1:
        values[0] = v
    else:
        values[0] = -v
    #stretch in the non equilibirum dashpot
    tau = abs(sigma_b(y[0]/y[1],[b10,b01]))
    if tau == 0:
        N=0.0
    else:
        N = sigma_b(y[0]/y[1],[b10,b01])/tau
    lambdaBp = np.sqrt((y[1]**3+2)/(3*y[1]))
    #the additional y[1] comes from the defintion of strain rate D as 
    #D = \dot{F}.F^{-1}
    values[1] = y[1]*A*((lambdaBp-1+E)**C)*(tau**m)*N

    return values    

def sigma_b(lb, params):
    """
    Returns the Cauchy stress in the non-equilibirum network given the stretch
    in the non linear spring.Can also be used for the equilibrium network with 
    the right parameters.

    Parameters
    -----------
    lb: float
        the stretch in the element

    params: list of floats
        the material parameters b10 and b01, in this order

    Returns
    --------
    sigma: float
        the Cauchy stress values

    """

    b10 = params[0]
    b01 = params[1]
    return 2*(b10+b01/lb)*(lb**2-1/lb)   
###########
#class
###########
class BB(Model):
    """
    """
    PARAMETER_NAMES = ['a10','a01', 'b10','b01','m','C','E','A']

    MAX_PARAM = len(PARAMETER_NAMES)

    RES_FUNC = {'loun':BB_loadunload,}

    NAME = 'BB'
